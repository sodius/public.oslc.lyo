# Introduction

This repository is an adaptation of [Eclipse Lyo](https://github.com/eclipse/lyo) 
made by [SodiusWillert](https://www.sodiuswillert.com/en/home)
to support multiple versions of [Apache Jena](https://jena.apache.org/)
and to no longer depend on retired [Apache Wink](http://wink.incubator.apache.org/index.html).

This repository forks a subset of Eclipse Lyo projects, namely:

* [oslc4j-jena-provider](https://github.com/eclipse/lyo/tree/master/core/oslc4j-jena-provider) - renamed as ``/com.sodius.oslc.core.provider.jena``
* [oslc4j-json4j-provider](https://github.com/eclipse/lyo/tree/master/core/oslc4j-json4j-provider) - renamed as ``/com.sodius.oslc.core.provider.json``

This repository delivers source code under [Eclipse Public License](LICENSE) (EPL).


# Getting started

Refer to [Eclipse Lyo](https://github.com/eclipse/lyo) site for an introduction of this library.

## Understanding the fork

SodiusWillert needs to design OSLC products that can be embedded in third-party applications
without strong requirements on the version of Apache Jena used, 
as a given version of this library can already be integrated in the application.

The design of this repository is:

1. Keep using [oslc4j-core](https://github.com/eclipse/lyo/tree/master/core/oslc4j-core) as delivered by Lyo community in OSLC products.
2. Use ``/com.sodius.oslc.core.provider.jena`` as a replacement of ``oslc4j-jena-provider`` delivered by Lyo community. Provider implementation does no longer directly access Jena APIs but goes through a ``/com.sodius.oslc.core.rdf`` RDF facade, so that various versions of Jena library can be used underneath. 
2. Use ``/com.sodius.oslc.core.provider.json`` as a replacement of ``oslc4j-json4j-provider`` delivered by Lyo community. Provider implementation does no longer use retired Apache Wink JSON4J but [Jakarta JSON Processing](https://eclipse-ee4j.github.io/jsonp/)
3. Provide ``/com.sodius.oslc.core.rdf.jena``**x** plug-ins for implementing RDF facades for various versions of Apache Jena. 

## Installing and importing the projects

This repository defines Eclipse projects and requires an [Eclipse IDE](https://www.eclipse.org/) for editing the source code.
Projects require the [Plug-in Development Environment](https://www.eclipse.org/pde/) to be installed in the Eclipse IDE.

Once your Eclipse IDE is installed and this GIT repository is cloned, you may import the Eclipse projects defined in this repository
in your environment.

## Compiling the source code

Eclipse projects use an Eclipse Target Platform for compiling the source code.
The target platform uses a local Eclipse update site for accessing third-party libraries packaged as OSGI bundles.

Steps to build the local Eclipse update site:

1. In Eclipse, click **Run > Run Configurations...** in the main menu.
2. Select the ``Build Update Site`` configuration and click **Run**.
3. Verify the execution created a ``/com.sodius.oslc.core.rdf.thirdparty.site/target/repository`` folder. This is a local Eclipse update site, with a ``plugins`` sub folder containing the third-party OSGI bundles.

Steps to setup the build environment:

1. In Eclipse, navigate to the ``/releng/target_platform/local.target`` file, which defines the target platform.
2. Right-click **Open With > Target Editor**.
3. Click the **Source** tab at the bottom of the editor, to edit the XML view of the file.
4. Modify the ``--git-local-folder--`` segment in the repository location, to reference your Eclipse local update site in your file system, as created with the steps above.
5. Click the **Definition** tab and ensure the Target Platform correctly resolves the OSGI bundles
6. Click **Set as Active Target Platform** on top right of the editor.

At this stage all projects should compile successfully.

# Contacting Us

Should any question or concern be raised, please [contact SodiusWillert](https://www.sodiuswillert.com/en/contact-us).