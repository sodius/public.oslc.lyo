package com.sodius.oslc.core.provider.jena.internal;

import java.util.function.Supplier;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Instantiating a TransformerFactory is very costly as it is called very often for large resources.
 * This supplier uses a cache for instantiating only once a TransformerFactory.
 * It also uses lazy instantiation so that we get no performance penalty when no Transformer is desired.
 *
 * As a side note, the Javadoc of TransformerFactory clearly states one should not assume an implementation
 * of TransformerFactory is thread-safe, hence the synchronized get() method.
 */
class TransformerSupplier implements Supplier<Transformer> {
    static final TransformerSupplier INSTANCE = new TransformerSupplier();

    private static final Logger LOGGER = LoggerFactory.getLogger(TransformerSupplier.class);

    private TransformerFactory transformerFactory;

    private TransformerSupplier() {
    }

    @Override
    public synchronized Transformer get() {
        try {
            if (transformerFactory == null) {
                transformerFactory = newTransformerFactory();
            }

            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes"); //$NON-NLS-1$
            return transformer;

        } catch (TransformerException e) {
            throw new RuntimeException(e); // NOSONAR
        }
    }

    /**
     * Returns a transformer factory configured to mitigate the risk of XML External Entity (XXE) attacks.
     *
     * @return a new transformer factory.
     */
    /*
     * See those guidelines:
     * https://find-sec-bugs.github.io/bugs.htm#XXE_DTD_TRANSFORM_FACTORY
     * https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html
     */
    private static TransformerFactory newTransformerFactory() {
        TransformerFactory factory = TransformerFactory.newInstance();

        // Secure processing protects against remote file access
        try {
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        } catch (TransformerConfigurationException e) {
            LOGGER.error(e.toString());
        }

        // Disabling DTD protects against remote file access
        try {
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); //$NON-NLS-1$
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.toString());
        }
        try {
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, ""); //$NON-NLS-1$
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.toString());
        }

        return factory;
    }
}