/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.jena.internal;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.namespace.QName;

import org.eclipse.lyo.oslc4j.core.NestedWildcardProperties;
import org.eclipse.lyo.oslc4j.core.OSLC4JConstants;
import org.eclipse.lyo.oslc4j.core.SingletonWildcardProperties;
import org.eclipse.lyo.oslc4j.core.UnparseableLiteral;
import org.eclipse.lyo.oslc4j.core.annotation.OslcRdfCollectionType;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreRelativeURIException;
import org.eclipse.lyo.oslc4j.core.model.AnyResource;
import org.eclipse.lyo.oslc4j.core.model.FilteredResource;
import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;
import org.eclipse.lyo.oslc4j.core.model.IReifiedResource;
import org.eclipse.lyo.oslc4j.core.model.IResource;
import org.eclipse.lyo.oslc4j.core.model.OslcConstants;
import org.eclipse.lyo.oslc4j.core.model.ResponseInfo;
import org.eclipse.lyo.oslc4j.core.model.TypeFactory;
import org.eclipse.lyo.oslc4j.core.model.ValueType;
import org.eclipse.lyo.oslc4j.core.model.XMLLiteral;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.sodius.oslc.core.provider.internal.JavaResourceShape;
import com.sodius.oslc.core.provider.internal.LyoProviderUtils;
import com.sodius.oslc.core.provider.internal.NamespaceMappings;
import com.sodius.oslc.core.provider.internal.PropertyAccessor;
import com.sodius.oslc.core.provider.internal.RdfCollections;
import com.sodius.oslc.core.provider.internal.ResourceShapes;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.RDFDataType;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.ReifiedStatement;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Statement;
import com.sodius.oslc.core.rdf.jena.JenaProvider;

@SuppressWarnings({ "java:S3776", "java:S1192", "java:S107" }) // Complex legacy class
public final class JenaModelBuilder {
    private static final String PROPERTY_TOTAL_COUNT = "totalCount";
    private static final String PROPERTY_NEXT_PAGE = "nextPage";

    private static final Logger LOGGER = LoggerFactory.getLogger(JenaModelBuilder.class);

    public static Model build(String descriptionAbout, String responseInfoAbout, ResponseInfo<?> responseInfo, Object[] objects,
            Map<String, Object> properties) throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, OslcCoreApplicationException {

        Model model = JenaProvider.models().create();
        JenaModelBuilder builder = new JenaModelBuilder(model);
        builder.buildModel(descriptionAbout, responseInfoAbout, responseInfo, objects, properties);
        return model;
    }

    private final Model model;
    private final NamespaceMappings namespaceMappings;
    private final RDFNodeFactory nodeFactory;

    private JenaModelBuilder(Model model) {
        this.model = model;
        this.namespaceMappings = NamespaceMappings.global();
        this.nodeFactory = new RDFNodeFactory(model);
    }

    private void buildModel(String descriptionAbout, String responseInfoAbout, ResponseInfo<?> responseInfo, Object[] objects,
            Map<String, Object> properties) throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, OslcCoreApplicationException {

        Resource descriptionResource = null;

        if (descriptionAbout != null) {
            if (LyoProviderUtils.isQueryResultListAsContainer()) {
                descriptionResource = model.createResource(descriptionAbout, JenaProvider.resources().RDFS_CONTAINER());
            } else {
                descriptionResource = model.createResource(descriptionAbout);
            }

            Map<IExtendedResource, Resource> visitedResources = new HashMap<>();
            buildExtendedProperties(FilteredResource.class, descriptionResource, responseInfo.getContainer(), properties, visitedResources);

            // response info is expected?
            if (responseInfoAbout != null) {
                Resource responseInfoResource = model.createResource(responseInfoAbout, model.createProperty(OslcConstants.TYPE_RESPONSE_INFO));

                // total count
                int totalCount = responseInfo.totalCount() == null ? objects.length : responseInfo.totalCount();
                responseInfoResource.addProperty(model.createProperty(OslcConstants.OSLC_CORE_NAMESPACE, PROPERTY_TOTAL_COUNT),
                        model.createTypedLiteral(totalCount));

                // next page
                if (responseInfo.nextPage() != null) {
                    responseInfoResource.addProperty(model.createProperty(OslcConstants.OSLC_CORE_NAMESPACE, PROPERTY_NEXT_PAGE),
                            model.createResource(responseInfo.nextPage()));
                }

                // response info properties
                visitedResources = new HashMap<>();
                buildExtendedProperties(ResponseInfo.class, responseInfoResource, responseInfo, properties, visitedResources);
            }
        }

        // build each resource
        for (Object object : objects) {
            buildResource(descriptionResource, object, properties);
        }

        // Ensure known namespace prefixes are registered
        if (descriptionAbout != null) {
            namespaceMappings.addKnownMapping(OslcConstants.RDF_NAMESPACE_PREFIX, OslcConstants.RDF_NAMESPACE);
            namespaceMappings.addKnownMapping(OslcConstants.RDFS_NAMESPACE_PREFIX, OslcConstants.RDFS_NAMESPACE);
            if (responseInfoAbout != null) {
                namespaceMappings.addKnownMapping(OslcConstants.OSLC_CORE_NAMESPACE_PREFIX, OslcConstants.OSLC_CORE_NAMESPACE);
            }
        }

        // Set the namespace prefixes
        namespaceMappings.getMappings().entrySet().forEach(mapping -> model.setNsPrefix(mapping.getKey(), mapping.getValue()));
    }

    private void buildResource(Resource descriptionResource, Object object, Map<String, Object> properties) throws DatatypeConfigurationException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException, OslcCoreApplicationException {

        // Collect the prefix/namespace mappings
        Class<? extends Object> objectClass = object.getClass();
        namespaceMappings.addMappings(objectClass);

        Resource mainResource;

        // uri?
        if (object instanceof URI) {
            mainResource = model.createResource(((URI) object).toASCIIString());
        }

        // resource
        else {

            // rdf:about
            URI aboutURI = null;
            if (object instanceof IResource) {
                aboutURI = ((IResource) object).getAbout();
            }
            if (aboutURI != null) {
                if (LyoProviderUtils.relativeURIsAreDisabled() && !aboutURI.isAbsolute()) {
                    throw new OslcCoreRelativeURIException(objectClass, "getAbout", aboutURI);
                }
                mainResource = model.createResource(aboutURI.toString());
            } else {
                mainResource = model.createResource();
            }

            // rdf:type based on the shape annotation
            if (objectClass.getAnnotation(OslcResourceShape.class) != null) {
                String qualifiedName = TypeFactory.getQualifiedName(objectClass);
                if (qualifiedName != null) {
                    mainResource.addProperty(JenaProvider.resources().RDF_TYPE(), model.createResource(qualifiedName));
                }
            }

            // all other properties
            buildResourceProperties(object, objectClass, mainResource, properties);
        }

        if (descriptionResource != null) {
            descriptionResource.addProperty(JenaProvider.resources().RDFS_MEMBER(), mainResource);
        }
    }

    /*
     * Build one resource, creating properties for each of its field values and extended properties
     */
    private void buildResourceProperties(Object object, Class<?> resourceClass, Resource mainResource, Map<String, Object> properties)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            OslcCoreApplicationException {

        if (properties == OSLC4JConstants.OSL4J_PROPERTY_SINGLETON) {
            return;
        }

        // loop on getters defined on the class
        for (PropertyAccessor accessor : JavaResourceShape.valueOf(resourceClass).getAccessors()) {

            // obtain the value from the getter
            Object value = accessor.getGetter().invoke(object);
            if (value != null) {

                Map<String, Object> nestedProperties = null;
                boolean onlyNested = false;

                if (properties != null) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) properties.get(accessor.getPropertyDefinition());

                    if (map != null) {
                        nestedProperties = map;
                    } else if ((properties instanceof SingletonWildcardProperties) && !(properties instanceof NestedWildcardProperties)) {
                        nestedProperties = OSLC4JConstants.OSL4J_PROPERTY_SINGLETON;
                    } else if (properties instanceof NestedWildcardProperties) {
                        nestedProperties = ((NestedWildcardProperties) properties).commonNestedProperties();
                        onlyNested = !(properties instanceof SingletonWildcardProperties);
                    } else {
                        continue;
                    }
                }

                buildAccessor(resourceClass, accessor, mainResource, value, nestedProperties, onlyNested);
            }
        }

        // extended properties
        if (object instanceof IExtendedResource) {
            Map<IExtendedResource, Resource> visitedResources = new HashMap<>();
            buildExtendedProperties(resourceClass, mainResource, (IExtendedResource) object, properties, visitedResources);
        }
    }

    /*
     * Add properties for the given extended resource, for its rdf types and extended properties
     */
    private void buildExtendedProperties(Class<?> resourceClass, Resource mainResource, IExtendedResource extendedResource,
            Map<String, Object> properties, Map<IExtendedResource, Resource> visitedResources)
            throws DatatypeConfigurationException, IllegalAccessException, InvocationTargetException, OslcCoreApplicationException {

        // keep track we visited that resource
        visitedResources.put(extendedResource, mainResource);

        // rdf:type based on the getter
        buildExtendedTypes(mainResource, extendedResource, properties);

        // loop on extended properties
        if (extendedResource.getExtendedProperties() != null) {
            for (Map.Entry<QName, ?> extendedProperty : extendedResource.getExtendedProperties().entrySet()) {
                QName qName = extendedProperty.getKey();
                namespaceMappings.addKnownMapping(qName);

                String propertyName = qName.getNamespaceURI() + qName.getLocalPart();
                Map<String, Object> nestedProperties = null;
                boolean onlyNested = false;

                if (properties != null) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) properties.get(propertyName);

                    if (map != null) {
                        nestedProperties = map;
                    } else if ((properties instanceof SingletonWildcardProperties) && !(properties instanceof NestedWildcardProperties)) {
                        nestedProperties = OSLC4JConstants.OSL4J_PROPERTY_SINGLETON;
                    } else if (properties instanceof NestedWildcardProperties) {
                        nestedProperties = ((NestedWildcardProperties) properties).commonNestedProperties();
                        onlyNested = !(properties instanceof SingletonWildcardProperties);
                    } else {
                        continue;
                    }
                }

                Property property = model.createProperty(propertyName);
                buildExtendedValue(resourceClass, extendedProperty.getValue(), mainResource, property, nestedProperties, onlyNested, visitedResources,
                        null);
            }
        }
    }

    private boolean isPropertyAccepted(Map<String, Object> properties, String propertyName) {
        return (properties == null) //
                || (properties.get(propertyName) != null) //
                || (properties instanceof NestedWildcardProperties) //
                || (properties instanceof SingletonWildcardProperties);
    }

    /*
     * Add properties for RDF types defined on the IExtendedResource
     */
    private void buildExtendedTypes(Resource mainResource, IExtendedResource extendedResource, Map<String, Object> properties) {
        if (!isPropertyAccepted(properties, JenaProvider.resources().RDF_TYPE().getURI())) {
            return;
        }

        if (extendedResource.getTypes() != null) {
            for (URI type : extendedResource.getTypes()) {
                String typeName = type.toString();

                Resource typeResource = model.createResource(typeName);
                if (!mainResource.hasProperty(JenaProvider.resources().RDF_TYPE(), typeResource)) {
                    mainResource.addProperty(JenaProvider.resources().RDF_TYPE(), typeResource);
                }
            }
        }
    }

    /*
     * Add a property for the given extended property value
     */
    private void buildExtendedValue(Class<?> objectClass, Object value, Resource resource, Property property, Map<String, Object> nestedProperties,
            boolean onlyNested, Map<IExtendedResource, Resource> visitedResources, NodeContainer rdfNodeContainer)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            OslcCoreApplicationException {

        // collection?
        if (value instanceof Collection) {
            Collection<?> collection = (Collection<?>) value;
            if (RdfCollections.isSequence(collection)) {
                rdfNodeContainer = new SequenceContainer(resource, property, nestedProperties);
            }
            for (Object collectionElement : collection) {
                buildExtendedValue(objectClass, collectionElement, resource, property, nestedProperties, onlyNested, visitedResources,
                        rdfNodeContainer);
            }
            if (rdfNodeContainer != null) {
                rdfNodeContainer.dispose();
            }
        }

        // AnyResource? (specifically, other resources should be handled with an OslcResourceShape below)
        else if (value instanceof AnyResource) {
            Resource nestedResource = buildExtendedAnyResource((AnyResource) value, nestedProperties, visitedResources);
            resource.addProperty(property, nestedResource);
        }

        // anything else (e.g. URI, boolean, number, class with OslcResourceShape annotation)
        else {
            // Until we handle XMLLiteral for incoming unknown resources, need to assume it is not XMLLiteral
            boolean xmlliteral = false;
            buildValue(objectClass, null, xmlliteral, value, resource, property, nestedProperties, onlyNested, rdfNodeContainer);
        }
    }

    /*
     * Create a resource for the given AnyResource
     */
    private Resource buildExtendedAnyResource(AnyResource value, Map<String, Object> nestedProperties,
            Map<IExtendedResource, Resource> visitedResources)
            throws DatatypeConfigurationException, IllegalAccessException, InvocationTargetException, OslcCoreApplicationException {

        // already visited?
        if (visitedResources.containsKey(value)) {
            return visitedResources.get(value);
        }

        else {
            // create a new resource
            Resource nestedResource;
            URI aboutURI = value.getAbout();
            if (aboutURI != null) {
                if (LyoProviderUtils.relativeURIsAreDisabled() && !aboutURI.isAbsolute()) {
                    throw new OslcCoreRelativeURIException(AnyResource.class, "getAbout", aboutURI);
                }
                nestedResource = model.createResource(aboutURI.toString());
            } else {
                nestedResource = model.createResource();
            }

            // rdf:type
            if (value.getTypes() != null) {
                for (URI type : value.getTypes()) {
                    String propertyName = type.toString();

                    if (isPropertyAccepted(nestedProperties, propertyName)) {
                        nestedResource.addProperty(JenaProvider.resources().RDF_TYPE(), model.createResource(propertyName));
                    }
                }
            }

            // extended properties
            buildExtendedProperties(AnyResource.class, nestedResource, value, nestedProperties, visitedResources);

            return nestedResource;
        }
    }

    /*
     * Add property for the value obtained with a getter on the resource
     */
    private void buildAccessor(Class<?> resourceClass, PropertyAccessor accessor, Resource resource, Object value,
            Map<String, Object> nestedProperties, boolean onlyNested) throws DatatypeConfigurationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException, OslcCoreApplicationException {

        boolean xmlLiteral = accessor.getValueType().isPresent() ? ValueType.XMLLiteral.equals(accessor.getValueType().get().value()) : false; // NOSONAR

        Property property = model.createProperty(accessor.getPropertyDefinition());

        Class<?> returnType = accessor.getGetter().getReturnType();
        OslcRdfCollectionType collectionType = accessor.getCollectionType().orElse(null);

        // create a container if the type is a RDF collection
        NodeContainer rdfNodeContainer;
        if ((collectionType != null) && RdfCollections.isRdfCollection(collectionType)) {
            if (RdfCollections.RDF_SEQ.equals(collectionType.collectionType())) {
                rdfNodeContainer = new SequenceContainer(resource, property, nestedProperties);
            } else {
                rdfNodeContainer = new UnorderedNodeContainer(resourceClass, resource, property, collectionType);
            }
        } else {
            rdfNodeContainer = null;
        }

        // array?
        if (returnType.isArray()) {
            // We cannot cast to Object[] in case this is an array of
            // primitives. We will use Array reflection instead.
            // Strange case about primitive arrays: they cannot be cast to
            // Object[], but retrieving their individual elements
            // via Array.get does not return primitives, but the primitive
            // object wrapping counterparts like Integer, Byte, Double, etc.
            int length = Array.getLength(value);

            for (int index = 0; index < length; index++) {
                Object object = Array.get(value, index);
                buildValue(resourceClass, accessor, xmlLiteral, object, resource, property, nestedProperties, onlyNested, rdfNodeContainer);
            }

            if (rdfNodeContainer != null) {
                rdfNodeContainer.dispose();
            }
        }

        // collection?
        else if (Collection.class.isAssignableFrom(returnType)) {
            @SuppressWarnings("unchecked")
            Collection<Object> collection = (Collection<Object>) value;
            for (Object object : collection) {
                buildValue(resourceClass, accessor, xmlLiteral, object, resource, property, nestedProperties, onlyNested, rdfNodeContainer);
            }

            if (rdfNodeContainer != null) {
                rdfNodeContainer.dispose();
            }
        }

        // anything else
        else {
            buildValue(resourceClass, accessor, xmlLiteral, value, resource, property, nestedProperties, onlyNested, null);
        }
    }

    private void buildValue(Class<?> resourceClass, PropertyAccessor accessor, boolean xmlLiteral, Object object, Resource resource,
            Property attribute, Map<String, Object> nestedProperties, boolean onlyNested, NodeContainer rdfNodeContainer)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            OslcCoreApplicationException {

        if (object == null) {
            return;
        }
        Class<?> objectClass = object.getClass();

        RDFNode nestedNode = null;
        IReifiedResource<?> reifiedResource = (object instanceof IReifiedResource) ? (IReifiedResource<?>) object : null;
        Object value = (reifiedResource == null) ? object : reifiedResource.getValue();
        if (value == null) {
            return;
        }

        // string?
        if (value instanceof String) {
            if (onlyNested) {
                return;
            }

            if (xmlLiteral) {
                nestedNode = model.createTypedLiteral(value.toString(), JenaProvider.dataTypes().XML_LITERAL());
            } else {
                nestedNode = model.createLiteral(value.toString());
            }
        }

        // float?
        else if (value instanceof Float) {
            if (onlyNested) {
                return;
            }
            nestedNode = nodeFactory.createLiteral((Float) value);
        }

        // double?
        else if (value instanceof Double) {
            if (onlyNested) {
                return;
            }
            nestedNode = nodeFactory.createLiteral((Double) value);
        }

        // boolean or number?
        else if ((value instanceof Boolean) || (value instanceof Number)) {
            if (onlyNested) {
                return;
            }

            nestedNode = model.createTypedLiteral(value);
        }

        // uri?
        else if (value instanceof URI) {
            if (onlyNested) {
                return;
            }

            URI uri = (URI) value;

            if (LyoProviderUtils.relativeURIsAreDisabled() && !uri.isAbsolute()) {
                throw new OslcCoreRelativeURIException(resourceClass, (accessor == null) ? "<none>" : accessor.getGetter().getName(), uri);
            }

            // URIs represent references to other resources identified by their IDs, so they need to be managed as such
            nestedNode = model.createResource(value.toString());
        }

        // date?
        else if (value instanceof Date) {
            if (onlyNested) {
                return;
            }

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime((Date) value);
            RDFDataType dataType = getDataType(resource, attribute);
            nestedNode = nodeFactory.createLiteral(calendar, dataType);
        }

        // resource whose class as a OslcResourceShape annotation?
        else if (objectClass.getAnnotation(OslcResourceShape.class) != null) {
            String namespace = TypeFactory.getNamespace(objectClass);
            String name = TypeFactory.getName(objectClass);

            URI aboutURI = null;
            if (value instanceof IResource) {
                aboutURI = ((IResource) value).getAbout();
            }

            Resource nestedResource;
            if (aboutURI != null) {
                if (LyoProviderUtils.relativeURIsAreDisabled() && !aboutURI.isAbsolute()) {
                    throw new OslcCoreRelativeURIException(objectClass, "getAbout", aboutURI);
                }

                nestedResource = model.createResource(aboutURI.toString(), model.createProperty(namespace, name));
            } else {
                nestedResource = model.createResource(model.createProperty(namespace, name));
            }

            buildResourceProperties(value, objectClass, nestedResource, nestedProperties);

            nestedNode = nestedResource;
        }

        // unparseable literal?
        else if (value instanceof UnparseableLiteral) {
            if (onlyNested) {
                return;
            }
            nestedNode = nodeFactory.createLiteral((UnparseableLiteral) value);
        }

        // xml literal?
        else if (value instanceof XMLLiteral) {
            if (onlyNested) {
                return;
            }
            nestedNode = nodeFactory.createLiteral((XMLLiteral) value);
        }

        // xml element?
        else if (value instanceof Element) {
            if (onlyNested) {
                return;
            }
            nestedNode = nodeFactory.createLiteral((Element) value);
        }

        // unsupported value
        else if (LOGGER.isWarnEnabled()) {
            // Warn that one of the properties could not be serialized because it does not have the right annotations.
            String subjectClassName = resourceClass.getSimpleName();
            if ("".equals(subjectClassName)) {
                subjectClassName = resourceClass.getName();
            }

            String objectClassName = objectClass.getSimpleName();
            if ("".equals(objectClassName)) {
                objectClassName = objectClass.getName();
            }

            String methodName = accessor == null ? "<none>" : accessor.getGetter().getName();

            LOGGER.warn("Could not serialize {} because it does not have an OslcResourceShape annotation (class: {}, method: {})", objectClassName,
                    subjectClassName, methodName);
        }

        if (nestedNode != null) {
            if (rdfNodeContainer != null) {
                rdfNodeContainer.accept(object, nestedNode);
            } else {
                Statement statement = model.createStatement(resource, attribute, nestedNode);

                if ((reifiedResource != null) && (nestedProperties != OSLC4JConstants.OSL4J_PROPERTY_SINGLETON)) {
                    buildReifiedStatements(statement, reifiedResource, nestedProperties);
                }

                // Finally, add the statement to the model.
                model.add(statement);
            }
        }
    }

    private void buildReifiedStatements(Statement statement, IReifiedResource<?> reifiedResource, Map<String, Object> nestedProperties)
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException,
            OslcCoreApplicationException {

        ReifiedStatement reifiedStatement = statement.createReifiedStatement();
        buildResourceProperties(reifiedResource, reifiedResource.getClass(), reifiedStatement, nestedProperties);

        // https://bugs.eclipse.org/bugs/show_bug.cgi?id=526188
        // If the resulting reifiedStatement only contain the 4 statements about its subject, predicate object, & type,
        // then there are no additional statements on the statement. Hence, remove the newly created reifiedStatement.
        if (reifiedStatement.listProperties().toList().size() == 4) {
            reifiedStatement.removeProperties();
            LOGGER.debug("An empty reified statement was stripped from the model");
        }
    }

    private static RDFDataType getDataType(Resource resource, Property property) {
        RDFDataType dataType = null;

        if (ResourceShapes.inferTypeFromShape()) {
            HashSet<String> rdfTypes = new HashSet<>();
            JenaUtils.collectTypesFromResource(resource, rdfTypes);
            dataType = ResourceShapes.getDataTypeBasedOnResourceShapeType(rdfTypes, property);
        }

        return dataType;
    }

    /*
     * A RDF collection container being notified of the nodes that are built and are to add in the collection.
     */
    private interface NodeContainer {

        /*
         * Notification a node was built and is to add in the underlying collection
         */
        public void accept(Object object, RDFNode node)
                throws IllegalAccessException, InvocationTargetException, DatatypeConfigurationException, OslcCoreApplicationException;

        /*
         * Notification all nodes are processed and this instance is no longer going to be used.
         */
        public void dispose();
    }

    /*
     * The container implementation for RDF Bag, List and Alt.
     * This implementation creates the underlying container only once all nodes are collected.
     * It does not support reified resources as container members.
     */
    private class UnorderedNodeContainer implements NodeContainer {

        private final Class<?> resourceClass;
        private final Resource resource;
        private final Property property;
        private final OslcRdfCollectionType collectionType;
        private final List<RDFNode> nodes;

        UnorderedNodeContainer(Class<?> resourceClass, Resource resource, Property property, OslcRdfCollectionType collectionType) {
            this.resourceClass = resourceClass;
            this.resource = resource;
            this.property = property;
            this.collectionType = collectionType;
            this.nodes = new ArrayList<>();
        }

        @Override
        public void accept(Object object, RDFNode node) {
            if (object instanceof IReifiedResource) {
                LOGGER.warn("Reified resource is not supported for RDF collection in: {}, property: {}", resourceClass.getClass().getName(),
                        property.getURI());
                return;
            }

            // Instead of adding a nested node to model, add it to a list
            nodes.add(node);
        }

        @Override
        public void dispose() {
            // creates the container with all collected nodes
            RDFNode container = nodeFactory.createContainer(collectionType, nodes);
            Statement s = model.createStatement(resource, property, container);
            model.add(s);
        }
    }

    /*
     * The container implementation for RDF sequences.
     * The underlying sequence is created when a first node is collected.
     * It supports reified resources as sequence members.
     */
    private class SequenceContainer implements NodeContainer {

        private final Resource resource;
        private final Property property;
        private final Map<String, Object> nestedProperties;
        private Resource sequence;
        private int sequenceSize;

        private SequenceContainer(Resource resource, Property property, Map<String, Object> nestedProperties) {
            this.resource = resource;
            this.property = property;
            this.nestedProperties = nestedProperties;
        }

        @Override
        public void accept(Object object, RDFNode node)
                throws IllegalAccessException, InvocationTargetException, DatatypeConfigurationException, OslcCoreApplicationException {

            // create the sequence on-demand
            if (sequence == null) {
                sequence = model.createResource();
                sequence.addProperty(JenaProvider.resources().RDF_TYPE(), JenaProvider.resources().RDF_SEQ());
                resource.addProperty(property, sequence);
            }

            // add the node as member
            Statement statement = model.createStatement(sequence, JenaProvider.resources().RDF_LI(++sequenceSize), node);
            model.add(statement);

            // build reified statements if needed
            if ((object instanceof IReifiedResource) && (nestedProperties != OSLC4JConstants.OSL4J_PROPERTY_SINGLETON)) {
                buildReifiedStatements(statement, (IReifiedResource<?>) object, nestedProperties);
            }
        }

        @Override
        public void dispose() {
            // nothing
        }
    }

}
