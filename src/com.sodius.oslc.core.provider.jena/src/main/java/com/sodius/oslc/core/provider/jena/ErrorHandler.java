/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.jena;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sodius.oslc.core.rdf.RDFErrorHandler;
import com.sodius.oslc.core.rdf.jena.JenaProvider;

public final class ErrorHandler implements RDFErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);
    private static final String JENA_RELATIVE_URI_WARNING_ID = "W130";

    @Override
    public void error(final Exception exception) {
        handleException(exception);
    }

    @Override
    public void fatalError(final Exception exception) {
        handleException(exception);
    }

    @Override
    public void warning(final Exception exception) {

        // Workaround to avoid flooding the logs with Jena warnings about using
        // relative URIs with no base URI. Common for reified statements in OSLC
        String msg = exception.getMessage();
        if ((msg != null) && (msg.indexOf(JENA_RELATIVE_URI_WARNING_ID) >= 0)) {
            LOGGER.info("Warning in Jena handling", exception);
        } else {
            LOGGER.warn("Warning in Jena handling", exception);
        }
    }

    private static void handleException(final Exception exception) {
        if (exception instanceof RuntimeException) {
            throw (RuntimeException) exception;
        }

        throw JenaProvider.models().wrapException(exception);
    }
}
