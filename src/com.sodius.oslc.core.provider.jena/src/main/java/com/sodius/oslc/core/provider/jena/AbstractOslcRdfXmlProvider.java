/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.jena;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Providers;

import org.eclipse.lyo.oslc4j.core.OSLC4JConstants;
import org.eclipse.lyo.oslc4j.core.annotation.OslcNotQueryResult;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.exception.MessageExtractor;
import org.eclipse.lyo.oslc4j.core.model.Error;
import org.eclipse.lyo.oslc4j.core.model.OslcMediaType;
import org.eclipse.lyo.oslc4j.core.model.ResponseInfo;
import org.eclipse.lyo.oslc4j.core.model.ResponseInfoArray;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sodius.oslc.core.provider.internal.LyoProviderUtils;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.RDFLanguages;

public abstract class AbstractOslcRdfXmlProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractOslcRdfXmlProvider.class);

    /**
     * System property {@value} : When "true", always abbreviate RDF/XML, even
     * when asked for application/rdf+xml. Otherwise, abbreviated RDF/XML is
     * only returned when application/xml is requested. Does not affect
     * text/turtle responses.
     *
     * @see RdfXmlAbbreviatedWriter
     */
    public static final String OSLC4J_ALWAYS_XML_ABBREV = "org.eclipse.lyo.oslc4j.alwaysXMLAbbrev";

    /**
     * System property {@value} : When "true" (default), fail on when reading a
     * property value that is not a legal instance of a datatype. When "false",
     * skip over invalid values in extended properties.
     */
    public static final String OSLC4J_STRICT_DATATYPES = "org.eclipse.lyo.oslc4j.strictDatatypes";

    private static final Annotation[] ANNOTATIONS_EMPTY_ARRAY = new Annotation[0];
    private static final Class<Error> CLASS_OSLC_ERROR = Error.class;

    private @Context HttpHeaders httpHeaders; // Available only on the server
    protected @Context HttpServletRequest httpServletRequest; // Available only on the server
    private @Context Providers providers; // Available on both client and server

    protected AbstractOslcRdfXmlProvider() {
        super();
    }

    @SuppressWarnings("java:S1172") // Can't change signature
    protected static boolean isWriteable(final Class<?> type, final Annotation[] annotations, final MediaType actualMediaType,
            final MediaType... requiredMediaTypes) {
        if ((type.getAnnotation(OslcResourceShape.class) != null) || (type.getAnnotation(OslcNotQueryResult.class) != null)) {
            // We do not have annotations when running from the non-web client.
            if (isCompatible(actualMediaType, requiredMediaTypes)) { // NOSONAR
                return true;
            }
        }

        return false;
    }

    @SuppressWarnings("java:S107") // Too many params in legacy method
    protected void writeTo(final Object[] objects, final MediaType baseMediaType, final MultivaluedMap<String, Object> map,
            final OutputStream outputStream, final Map<String, Object> properties, final String descriptionURI, final String responseInfoURI,
            final ResponseInfo<?> responseInfo) throws WebApplicationException {
        String serializationLanguage = getSerializationLanguage(baseMediaType);

        // This is a special case to handle RDNG GET on a ServiceProvider resource.
        // RDNG can only consume RDF/XML-ABBREV although its sometimes sends Accept=application/rdf+xml.
        // The org.eclipse.lyo.oslc4j.alwaysXMLAbbrevOnlyProviders system property is used
        // to turn off this special case
        if (((objects != null) && (objects[0] != null)) && ((objects[0] instanceof ServiceProviderCatalog) || (objects[0] instanceof ServiceProvider))
                && serializationLanguage.equals(RDFLanguages.XML)
                && "true".equals(System.getProperty("org.eclipse.lyo.oslc4j.alwaysXMLAbbrevOnlyProviders"))) {
            serializationLanguage = RDFLanguages.XML_ABBREV;
            LOGGER.info("Using RDF/XML-ABBREV for ServiceProvider resources");
        }

        try {
            final Model model = JenaModelHelper.createJenaModel(descriptionURI, responseInfoURI, responseInfo, objects, properties);
            JenaIO.write(model, outputStream, Optional.of(serializationLanguage));
        } catch (final Exception exception) { // NOSONAR
            LOGGER.warn(MessageExtractor.getMessage("ErrorSerializingResource"), exception);
            throw new WebApplicationException(exception, buildServerErrorResponse(exception, baseMediaType, map));
        }
    }

    protected void writeTo(final boolean queryResult, final Object[] objects, final MediaType baseMediaType, final MultivaluedMap<String, Object> map,
            final OutputStream outputStream) throws WebApplicationException {
        boolean isClientSide = false;

        try {
            httpServletRequest.getMethod();
        } catch (RuntimeException e) {
            isClientSide = true;
        }

        String descriptionURI = null;
        String responseInfoURI = null;

        if (queryResult && !isClientSide) {

            final String method = httpServletRequest.getMethod();
            if ("GET".equals(method)) {
                descriptionURI = LyoProviderUtils.resolveURI(httpServletRequest);
                responseInfoURI = descriptionURI;

                final String queryString = httpServletRequest.getQueryString();
                if ((queryString != null) && (isOslcQuery(queryString))) {
                    responseInfoURI += "?" + queryString;
                }
            }

        }

        final String serializationLanguage = getSerializationLanguage(baseMediaType);

        @SuppressWarnings("unchecked")
        final Map<String, Object> properties = isClientSide ? null
                : (Map<String, Object>) httpServletRequest.getAttribute(OSLC4JConstants.OSLC4J_SELECTED_PROPERTIES);
        final String nextPageURI = isClientSide ? null : (String) httpServletRequest.getAttribute(OSLC4JConstants.OSLC4J_NEXT_PAGE);
        final Integer totalCount = isClientSide ? null : (Integer) httpServletRequest.getAttribute(OSLC4JConstants.OSLC4J_TOTAL_COUNT);

        ResponseInfo<?> responseInfo = new ResponseInfoArray<>(null, properties, totalCount, nextPageURI);

        try {
            final Model model = JenaModelHelper.createJenaModel(descriptionURI, responseInfoURI, responseInfo, objects, properties);
            JenaIO.write(model, outputStream, Optional.of(serializationLanguage));
        } catch (final Exception exception) { // NOSONAR
            LOGGER.warn(MessageExtractor.getMessage("ErrorSerializingResource"), exception);
            throw new WebApplicationException(exception, buildServerErrorResponse(exception, baseMediaType, map));
        }
    }

    private String getSerializationLanguage(final MediaType baseMediaType) {
        // Determine whether to serialize in xml or abreviated xml based upon mediatype.
        // application/rdf+xml yields xml
        // applicaton/xml yields abbreviated xml
        if (OslcMediaType.TEXT_TURTLE_TYPE.equals(baseMediaType)) {
            return RDFLanguages.TURTLE;
        }

        if (OslcMediaType.APPLICATION_JSON_LD_TYPE.isCompatible(baseMediaType)) {
            return RDFLanguages.JSONLD;
        }

        if (OslcMediaType.APPLICATION_RDF_XML_TYPE.isCompatible(baseMediaType) && !"true".equals(System.getProperty(OSLC4J_ALWAYS_XML_ABBREV))) {
            return RDFLanguages.XML;
        }

        return RDFLanguages.XML_ABBREV;
    }

    protected static boolean isReadable(final Class<?> type, final MediaType actualMediaType, final MediaType... requiredMediaTypes) {
        if (type.getAnnotation(OslcResourceShape.class) != null) {
            if (isCompatible(actualMediaType, requiredMediaTypes)) { // NOSONAR
                return true;
            }
        }

        return false;
    }

    protected static boolean isCompatible(final MediaType actualMediaType, final MediaType... requiredMediaTypes) {
        for (final MediaType requiredMediaType : requiredMediaTypes) {
            if (requiredMediaType.isCompatible(actualMediaType)) {
                return true;
            }
        }

        return false;
    }

    protected Object[] readFrom(final Class<?> type, final MediaType mediaType, final MultivaluedMap<String, String> map,
            final InputStream inputStream) throws WebApplicationException {

        String language = getSerializationLanguage(mediaType);

        try {
            Model model = JenaIO.read(inputStream, Optional.of(language));
            return JenaModelHelper.fromJenaModel(model, type);
        } catch (final Exception exception) {
            throw new WebApplicationException(exception, buildBadRequestResponse(exception, mediaType, map));
        }
    }

    protected Response buildBadRequestResponse(final Exception exception, final MediaType initialErrorMediaType,
            final MultivaluedMap<String, ?> map) {
        return buildResponse(exception, initialErrorMediaType, map, Response.Status.BAD_REQUEST);
    }

    protected Response buildServerErrorResponse(final Exception exception, final MediaType initialErrorMediaType,
            final MultivaluedMap<String, ?> map) {
        return buildResponse(exception, initialErrorMediaType, map, Response.Status.INTERNAL_SERVER_ERROR);
    }

    private Response buildResponse(final Exception exception, final MediaType errorMediaType, final MultivaluedMap<String, ?> map,
            Response.Status status) {
        try {
            final MediaType determinedErrorMediaType = determineErrorMediaType(errorMediaType, map);

            final Error error = new Error();

            error.setStatusCode(String.valueOf(status.getStatusCode()));
            error.setMessage(exception.getMessage());

            final ResponseBuilder responseBuilder = Response.status(status);
            return responseBuilder.type(determinedErrorMediaType).entity(error).build();
        } catch (UndeclaredThrowableException e) {
            // determineErrorMediaType may raise an UndeclaredThrowableException with an infinite stack trace
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * We handle the case where a client requests an accept type different than their content type.
     * This will work correctly in the successful case. But, in the error case where our
     * MessageBodyReader/MessageBodyWriter fails internally, we respect the client's requested accept type.
     */
    @SuppressWarnings("java:S3776") // Complex legacy method
    private MediaType determineErrorMediaType(final MediaType initialErrorMediaType, final MultivaluedMap<String, ?> map) {
        try {
            // HttpHeaders will not be available on the client side and will throw a NullPointerException
            final List<MediaType> acceptableMediaTypes = httpHeaders.getAcceptableMediaTypes();

            // Since we got here, we know we are executing on the server

            if (acceptableMediaTypes != null) {
                for (final MediaType acceptableMediaType : acceptableMediaTypes) {
                    // If a concrete media type with a MessageBodyWriter
                    if (isAcceptableMediaType(acceptableMediaType)) {
                        return acceptableMediaType;
                    }
                }
            }
        } catch (final NullPointerException exception) {
            // Ignore NullPointerException since this means the context has not been set since we are on the client

            // See if the MultivaluedMap for headers is available
            if (map != null) {
                final Object acceptObject = map.getFirst("Accept");

                if (acceptObject instanceof String) {
                    final String[] accepts = acceptObject.toString().split(",");

                    double winningQ = 0.0D;
                    MediaType winningMediaType = null;

                    for (final String accept : accepts) {
                        final MediaType acceptMediaType = MediaType.valueOf(accept);

                        // If a concrete media type with a MessageBodyWriter
                        if (isAcceptableMediaType(acceptMediaType)) {
                            final String stringQ = acceptMediaType.getParameters().get("q");

                            final double q = stringQ == null ? 1.0D : Double.parseDouble(stringQ);

                            // Make sure and exclude blackballed media type
                            if (Double.compare(q, 0.0D) > 0) {
                                if ((winningMediaType == null) || (Double.compare(q, winningQ) > 0)) { // NOSONAR
                                    winningMediaType = acceptMediaType;
                                    winningQ = q;
                                }
                            }
                        }
                    }

                    if (winningMediaType != null) {
                        return winningMediaType;
                    }
                }
            }
        }

        return initialErrorMediaType;
    }

    /**
     * Only allow media types that are not wildcards and have a related MessageBodyWriter for OSLC Error.
     */
    private boolean isAcceptableMediaType(final MediaType mediaType) {
        return (!mediaType.isWildcardType()) && (!mediaType.isWildcardSubtype())
                && (providers.getMessageBodyWriter(CLASS_OSLC_ERROR, CLASS_OSLC_ERROR, ANNOTATIONS_EMPTY_ARRAY, mediaType) != null);
    }

    protected static boolean isOslcQuery(final String parmString) {
        boolean containsOslcParm = false;

        final String[] uriParts = parmString.toLowerCase(Locale.getDefault()).split("oslc\\.", 2);
        if (uriParts.length > 1) {
            containsOslcParm = true;
        }

        return containsOslcParm;
    }
}