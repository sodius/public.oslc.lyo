package com.sodius.oslc.core.provider.jena.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.lyo.oslc4j.core.model.OslcConstants;

import com.sodius.oslc.core.provider.internal.RdfCollections;
import com.sodius.oslc.core.provider.internal.ResourceShapes;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Statement;
import com.sodius.oslc.core.rdf.jena.JenaProvider;

class JenaUtils {
    static final String RDF_TYPE_URI = OslcConstants.RDF_NAMESPACE + "type";

    static boolean isRdfCollection(RDFNode object) {
        if (object.isResource()) {
            Resource resource = object.asResource();
            Model model = object.getModel();
            if (resource.hasProperty(JenaProvider.resources().RDF_TYPE(), model.getResource(RDF_TYPE_URI + RdfCollections.RDF_ALT))
                    || resource.hasProperty(JenaProvider.resources().RDF_TYPE(), model.getResource(RDF_TYPE_URI + RdfCollections.RDF_BAG))
                    || resource.hasProperty(JenaProvider.resources().RDF_TYPE(), model.getResource(RDF_TYPE_URI + RdfCollections.RDF_SEQ))) {
                return true;
            }
        }

        return false;
    }

    static boolean isRdfSeq(RDFNode object) {
        if (object.isResource()) {
            Resource resource = object.asResource();
            Model model = object.getModel();
            if (resource.hasProperty(JenaProvider.resources().RDF_TYPE(), model.getResource(OslcConstants.RDF_NAMESPACE + RdfCollections.RDF_SEQ))) {
                return true;
            }
        }

        return false;
    }

    static void collectTypesFromResource(Resource resource, Set<String> types) {
        // The list of rdf:types will be populated only if the property
        // inferTypeFromShape is set and if the list was not populated before.
        // This is necessary because for an inline resource, the retuned
        // rdf:type is not from the parent resource, it is from the actual
        // resource.
        if (ResourceShapes.inferTypeFromShape() && types.isEmpty()) {
            Iterator<Statement> rdfTypesIterator = resource.listProperties(JenaProvider.resources().RDF_TYPE());
            while (rdfTypesIterator.hasNext()) {
                Statement rdfTypeStmt = rdfTypesIterator.next();
                RDFNode object = rdfTypeStmt.getObject();
                if (object.isResource()) {
                    String rdfType = object.asResource().getURI();
                    types.add(rdfType);
                }
            }
        }
    }

    /*
     * Returns the language of the literal value of the statement, if any.
     * This method returns false if either the value is not a literal or the literal is not declaring xml:lang="something"
     */
    static Optional<String> getLiteralLanguage(Statement statement) {
        if (statement.getObject().isLiteral()) {
            String language = statement.getObject().asLiteral().getLanguage();
            if ((language != null) && (language.length() != 0)) {
                return Optional.of(language);
            }
        }

        return Optional.empty();
    }

    /*
     * Return statements of the resource ordered by the language of their (literal) value (PRDOSLC-659).
     *
     * RDF allows using triplets where the value sets a language. The intent is to provide translations for a value.
     * When databinding statements to a Java instance, we want to order statements so that:
     * 1) We give priority to the literal not declaring a language (default translation)
     * 2) If not found, we give priority to the English literal (using xml:lang="en")
     * 3) If not found, we use whatever value comes in first
     */
    static List<Statement> getLanguageOrderedStatements(Resource resource) {
        // Use LinkedList as it's the fastest implementation when removing elements from the list
        LinkedList<Statement> remainingStatements = new LinkedList<>(resource.listProperties().toList());

        List<Statement> orderedStatements = new ArrayList<>(remainingStatements.size());

        // 1) add first all statements non using a language (i.e. default translations)
        for (Iterator<Statement> i = remainingStatements.iterator(); i.hasNext();) {
            Statement statement = i.next();
            Optional<String> language = JenaUtils.getLiteralLanguage(statement);
            if (!language.isPresent()) {
                orderedStatements.add(statement);
                i.remove(); // statement is added, no need to loop on it again
            }
        }

        // 2) add second all English statements
        for (Iterator<Statement> i = remainingStatements.iterator(); i.hasNext();) {
            Statement statement = i.next();
            Optional<String> language = JenaUtils.getLiteralLanguage(statement);
            if ("en".equals(language.orElse(""))) {
                orderedStatements.add(statement);
                i.remove(); // statement is added, no need to loop on it again
            }
        }

        // 3) add last all other languages statements (no priority between two non English languages)
        orderedStatements.addAll(remainingStatements);

        return orderedStatements;
    }

    private JenaUtils() {
    }

}
