/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.jena.internal;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;

import org.eclipse.lyo.oslc4j.core.UnparseableLiteral;
import org.eclipse.lyo.oslc4j.core.annotation.OslcResourceShape;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreMisusedOccursException;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreRelativeURIException;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.AnyResource;
import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;
import org.eclipse.lyo.oslc4j.core.model.IReifiedResource;
import org.eclipse.lyo.oslc4j.core.model.IResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.OslcConstants;
import org.eclipse.lyo.oslc4j.core.model.TypeFactory;
import org.eclipse.lyo.oslc4j.core.model.XMLLiteral;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sodius.oslc.core.provider.internal.CollectionSetterInvoker;
import com.sodius.oslc.core.provider.internal.JavaResourceShape;
import com.sodius.oslc.core.provider.internal.LyoProviderUtils;
import com.sodius.oslc.core.provider.internal.NamespaceMappings;
import com.sodius.oslc.core.provider.internal.PropertyAccessor;
import com.sodius.oslc.core.provider.internal.RdfCollections;
import com.sodius.oslc.core.provider.internal.ResourceShapes;
import com.sodius.oslc.core.provider.internal.Uris;
import com.sodius.oslc.core.provider.jena.AbstractOslcRdfXmlProvider;
import com.sodius.oslc.core.rdf.ExtendedIterator;
import com.sodius.oslc.core.rdf.Literal;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.RDFDataType;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.ReifiedStatement;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Statement;
import com.sodius.oslc.core.rdf.jena.JenaProvider;

@SuppressWarnings("java:S3776") // Complex legacy class
public final class ResourceBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceBuilder.class);

    private static final String TYPE_SEQ = OslcConstants.RDF_NAMESPACE + RdfCollections.RDF_SEQ;
    private static final String PROPERTY_DCTERMS_TITLE = OslcConstants.DCTERMS_NAMESPACE + "title";

    public static Object build(Resource resource, Class<?> beanClass)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InstantiationException,
            InvocationTargetException, OslcCoreApplicationException, URISyntaxException, SecurityException, NoSuchMethodException {

        ResourceBuilder builder = new ResourceBuilder(resource.getModel());
        return builder.buildResource(beanClass, resource);
    }

    public static Object[] build(Model model, Class<?> beanClass)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InstantiationException,
            InvocationTargetException, OslcCoreApplicationException, URISyntaxException, SecurityException, NoSuchMethodException {

        // class has a OslcResourceShape annotation?
        if (beanClass.getAnnotation(OslcResourceShape.class) != null) {

            // Fix for defect 412755
            // keep the same behavior, i.e. use the class name to match the resource rdf:type
            if (!LyoProviderUtils.useBeanClassForParsing()) {

                String qualifiedName = TypeFactory.getQualifiedName(beanClass);
                List<Resource> resourceList = model.listSubjectsWithProperty(JenaProvider.resources().RDF_TYPE(), model.getResource(qualifiedName))
                        .toList();

                return new ResourceBuilder(model).buildResources(beanClass, resourceList);
            } else {
                // get the list of subjects that have rdf:type element
                ExtendedIterator<Resource> listSubjects = model.listSubjectsWithProperty(JenaProvider.resources().RDF_TYPE());

                List<Resource> resourceList = new ArrayList<>();

                // iterate over the list of subjects to create a list of
                // subjects that does not contain inline resources
                while (listSubjects.hasNext()) {
                    Resource resource = listSubjects.next();

                    if (!isInline(model, resource)) {
                        // the current resource is not an inline resource, it
                        // should be considered in the list of subjects
                        resourceList.add(resource);
                    }
                }

                return new ResourceBuilder(model).buildResources(beanClass, resourceList);
            }
        }

        // URIs? will return rdfs:member objects
        else if (URI.class.equals(beanClass)) {
            return buildMemberURIs(model);
        }

        else {
            return new Object[0];
        }
    }

    /*
     * Check if the resource is an inline resource, i.e it has a parent node
     */
    private static boolean isInline(Model model, Resource resource) {
        return model.listStatements(null, null, resource).hasNext();
    }

    private static Object[] buildMemberURIs(Model model) throws URISyntaxException {
        List<URI> results = new ArrayList<>();
        Iterator<Statement> memberIterator = model.listStatements(null, JenaProvider.resources().RDFS_MEMBER(), (RDFNode) null);
        while (memberIterator.hasNext()) {
            Statement memberStatement = memberIterator.next();
            RDFNode memberObject = memberStatement.getObject();
            if (memberObject.isURIResource()) {
                URI memberURI = Uris.create(memberObject.asResource().getURI());
                results.add(memberURI);
            }
        }
        return results.toArray(new URI[results.size()]);
    }

    private final NamespaceMappings namespaceMappings;
    private final Map<String, Object> visitedResources;

    private ResourceBuilder(Model model) {
        this.namespaceMappings = NamespaceMappings.of(model.getNsPrefixMap(), model::setNsPrefix);
        this.visitedResources = new HashMap<>();
    }

    private Object[] buildResources(Class<?> beanClass, List<Resource> resourceList) throws IllegalAccessException, InstantiationException,
            DatatypeConfigurationException, InvocationTargetException, OslcCoreApplicationException, URISyntaxException, NoSuchMethodException {

        // build the resources
        List<Object> results = new ArrayList<>();
        for (Resource resource : resourceList) {
            results.add(buildResource(beanClass, resource));
        }

        // prevent cycles
        new PreventResourceCycles(results).run();

        return results.toArray((Object[]) Array.newInstance(beanClass, results.size()));
    }

    private Object buildResource(Class<?> beanClass, Resource resource)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InstantiationException,
            InvocationTargetException, OslcCoreApplicationException, URISyntaxException, SecurityException, NoSuchMethodException {

        Object newInstance = beanClass.getDeclaredConstructor().newInstance();
        Set<String> rdfTypes = new HashSet<>();
        buildResource(beanClass, newInstance, resource, rdfTypes);
        return newInstance;
    }

    private void buildResource(Class<?> beanClass, Object bean, Resource resource, Set<String> rdfTypes)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InstantiationException,
            InvocationTargetException, OslcCoreApplicationException, URISyntaxException, SecurityException, NoSuchMethodException {

        // keep track the resource is visited
        markVisited(resource, bean);

        // rdf:about
        if (bean instanceof IResource) {
            String aboutURIString = resource.getURI();
            if (aboutURIString != null) {
                URI aboutURI = Uris.create(aboutURIString);
                if (aboutURI.isAbsolute()) {
                    ((IResource) bean).setAbout(aboutURI);
                }
                // ignore relative URIs when creating new non-local resources
                else if (!(bean instanceof AbstractResource)) {
                    throw new OslcCoreRelativeURIException(beanClass, "setAbout", aboutURI);
                }
            }
        }

        // build statements
        CollectionSetterInvoker collectionSetterInvoker = new CollectionSetterInvoker(bean);
        buildStatements(beanClass, bean, resource, rdfTypes, collectionSetterInvoker);

        // Now, handle array and collection values since all are collected.
        collectionSetterInvoker.invokeAll();
    }

    private void buildStatements(Class<?> beanClass, Object bean, Resource resource, Set<String> rdfTypes,
            CollectionSetterInvoker collectionSetterInvoker) throws URISyntaxException, DatatypeConfigurationException, IllegalAccessException,
            InstantiationException, InvocationTargetException, OslcCoreApplicationException, NoSuchMethodException {

        // Ensure a single-value property is not set more than once
        Set<Method> singleValueMethodsUsed = new HashSet<>();

        IExtendedResource extendedResource;
        Map<QName, Object> extendedProperties;
        if (bean instanceof IExtendedResource) {
            extendedResource = (IExtendedResource) bean;
            extendedProperties = new HashMap<>();
            extendedResource.setExtendedProperties(extendedProperties);
        } else {
            extendedResource = null;
            extendedProperties = null;
        }

        // get the list of resource rdf type
        JenaUtils.collectTypesFromResource(resource, rdfTypes);

        // loop on statements (order based on their language)
        for (Statement statement : JenaUtils.getLanguageOrderedStatements(resource)) {

            Property predicate = statement.getPredicate();
            RDFNode object = statement.getObject();
            String predicateUri = predicate.getURI();
            Optional<PropertyAccessor> accessor = JavaResourceShape.valueOf(bean.getClass()).getAccessor(predicateUri);

            // no setter for this property?
            if (!accessor.isPresent()) {

                // rdf:type?
                if (JenaUtils.RDF_TYPE_URI.equals(predicateUri)) {
                    if (extendedResource != null) {
                        URI type = Uris.create(object.asResource().getURI());
                        if (extendedResource.getTypes() == null) {
                            extendedResource.setTypes(new ArrayList<>());
                        }
                        if (!extendedResource.getTypes().contains(type)) {
                            extendedResource.addType(type);
                        }
                    }
                    // Otherwise ignore missing propertyDefinition for rdf:type.
                }

                // any other predicate
                else {
                    // not an extended resource?
                    if (extendedProperties == null) {

                        // log a debug message, unless this is a reified resource and the property is subject/predicate/object
                        // @formatter:off
                        if (LOGGER.isDebugEnabled() && !((bean instanceof IReifiedResource) &&
                                (predicate.equals(JenaProvider.resources().RDF_SUBJECT())
                                || predicate.equals(JenaProvider.resources().RDF_PREDICATE())
                                || predicate.equals(JenaProvider.resources().RDF_OBJECT())))) {

                            LOGGER.debug("Set method not found for object type: {}, uri: {}", beanClass.getName(), predicateUri);
                        }
                        // @formatter:on
                    } else {
                        // register the value as an extended property
                        QName key = this.namespaceMappings.generateQName(predicate.getNameSpace(), predicate.getLocalName());
                        Object value = buildExtendedValue(statement, beanClass, object, key, rdfTypes);
                        addExtendedProperty(extendedProperties, key, value);
                    }
                }
            }

            // use the setter
            else {

                // determine whether a collection is expected and the type of element to set
                Method setMethod = accessor.get().getSetter();
                Class<?> setMethodComponentParameterClass = setMethod.getParameterTypes()[0];
                boolean multiple = setMethodComponentParameterClass.isArray();
                if (multiple) {
                    setMethodComponentParameterClass = setMethodComponentParameterClass.getComponentType();
                } else if (Collection.class.isAssignableFrom(setMethodComponentParameterClass)) {
                    multiple = true;

                    Type genericParameterType = setMethod.getGenericParameterTypes()[0];
                    if (genericParameterType instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) genericParameterType;
                        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                        if (actualTypeArguments.length == 1) {
                            Type actualTypeArgument = actualTypeArguments[0];
                            if (actualTypeArgument instanceof Class) {
                                setMethodComponentParameterClass = (Class<?>) actualTypeArgument;
                            }
                        }
                    }
                }

                List<RDFNode> objects;
                Function<RDFNode, List<ReifiedStatement>> reifiedStatementProvider = rdfNode -> statement.listReifiedStatements().toList();

                if (multiple && object.isResource()
                        && ((object.asResource().hasProperty(JenaProvider.resources().RDF_FIRST())
                                && object.asResource().hasProperty(JenaProvider.resources().RDF_REST()))
                                || (JenaProvider.resources().RDF_NIL().equals(object)) || object.canAsList())) {
                    objects = new ArrayList<>();

                    if (JenaProvider.resources().RDF_NIL().equals(object)) {
                        objects.add(JenaProvider.resources().RDF_NIL());
                    } else {
                        Resource listNode = object.asResource();
                        while ((listNode != null) && !JenaProvider.resources().RDF_NIL().getURI().equals(listNode.getURI())) {
                            markVisited(listNode, new Object());

                            RDFNode o = listNode.getPropertyResourceValue(JenaProvider.resources().RDF_FIRST());
                            objects.add(o);

                            listNode = listNode.getPropertyResourceValue(JenaProvider.resources().RDF_REST());
                        }
                    }

                    markVisited(object.asResource(), objects);
                }

                // rdf:Seq?
                else if (multiple && JenaUtils.isRdfSeq(object)) {
                    objects = buildCollectionNodes(object.asSeqMembers());
                    markVisited(object.asResource(), objects);

                    // Change the provider of reified statements, so that it uses the statement where rdf:Seq is the subject
                    // and the given RDFNode (the sequence member) is the object.
                    // This is where reified statements can be found, notably dcterms:title
                    // It is to correctly cover the case of a sequence of Links
                    reifiedStatementProvider = rdfNode -> object.getModel() //
                            .listStatements(object.asResource(), null, rdfNode).toList().stream() // there should be only one statement
                            .flatMap(s -> s.listReifiedStatements().toList().stream()) //
                            .collect(Collectors.toList());
                }

                /// other RDF collection?
                else if (multiple && JenaUtils.isRdfCollection(object)) {
                    ExtendedIterator<RDFNode> iterator = object.asResource().listProperties(JenaProvider.resources().RDFS_MEMBER())
                            .mapWith(s -> s.getObject());
                    objects = buildCollectionNodes(iterator);
                    markVisited(object.asResource(), objects);
                }

                // unique object
                else {
                    objects = Collections.singletonList(object);
                }

                // setter type is a reified resource?
                Class<?> reifiedClass = null;
                if (IReifiedResource.class.isAssignableFrom(setMethodComponentParameterClass)) {
                    reifiedClass = setMethodComponentParameterClass;
                    Type genericType = setMethodComponentParameterClass.getGenericSuperclass();

                    if (genericType instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) genericType;
                        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                        if (actualTypeArguments.length == 1) {
                            Type actualTypeArgument = actualTypeArguments[0];
                            if (actualTypeArgument instanceof Class) {
                                setMethodComponentParameterClass = (Class<?>) actualTypeArgument;
                            }
                        }
                    }
                }

                // loop on nodes
                for (RDFNode o : objects) {

                    // build the argument for the setter
                    Object parameter = buildAccessor(beanClass, accessor.get(), setMethodComponentParameterClass, o, rdfTypes);
                    if (parameter != null) {

                        // If property supports reified statements, build a reified resource
                        if (reifiedClass != null) {
                            List<ReifiedStatement> reifiedStatements = reifiedStatementProvider.apply(o);
                            parameter = buildAccessorReifiedResource(reifiedStatements, reifiedClass, setMethodComponentParameterClass, parameter,
                                    rdfTypes);
                        }

                        // array expected?
                        if (multiple) {
                            collectionSetterInvoker.add(accessor.get(), parameter);
                        }

                        else {
                            // a value is yet assigned for this single valued property?
                            if (singleValueMethodsUsed.contains(setMethod)) {

                                // Log an error if OslcOccurs is not correctly used (PRDOSLC-60),
                                // but only in the case a language is not set on this literal value (PRDOSLC-659).
                                // As we order statements based on languages, if a value is yet assigned for this property:
                                // - if no language is set, means either the Java class or RDF content is incorrect, report an error
                                // - if a language is set, means another translation with higher priority was assigned, this one can be safely ignored
                                if (!JenaUtils.getLiteralLanguage(statement).isPresent()) {
                                    OslcCoreMisusedOccursException e = new OslcCoreMisusedOccursException(beanClass, setMethod);
                                    LOGGER.warn(e.getMessage());
                                }
                            }

                            // call the setter
                            else {
                                setMethod.invoke(bean, parameter);
                                singleValueMethodsUsed.add(setMethod);
                            }
                        }
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void addExtendedProperty(Map<QName, Object> extendedProperties, QName key, Object value) {
        Object previous = extendedProperties.get(key);
        if (previous == null) {
            extendedProperties.put(key, value);
        } else {
            Collection<Object> collection;
            if (previous instanceof Collection) {
                collection = ((Collection<Object>) previous);
            } else {
                collection = new ArrayList<>();
                collection.add(previous);
                extendedProperties.put(key, collection);
            }
            collection.add(value);
        }
    }

    private List<RDFNode> buildCollectionNodes(Iterator<RDFNode> nodes) {
        List<RDFNode> objects = new ArrayList<>();
        while (nodes.hasNext()) {
            RDFNode o = nodes.next();

            if (o.isResource()) {
                markVisited(o.asResource(), new Object());
            }

            objects.add(o);
        }
        return objects;
    }

    private Object buildAccessor(Class<?> beanClass, PropertyAccessor accessor, Class<?> type, RDFNode node, Set<String> rdfTypes)
            throws DatatypeConfigurationException, URISyntaxException, InstantiationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, SecurityException, NoSuchMethodException, OslcCoreApplicationException {

        // node is a literal?
        if (node.isLiteral()) {
            return buildAccessorLiteral(node.asLiteral().getString(), type);
        }

        // node is a resource
        else if (node.isResource()) {
            Resource nestedResource = node.asResource();

            // uri is expected?
            if (URI.class == type) {
                String nestedResourceURIString = nestedResource.getURI();
                if (nestedResourceURIString != null) {
                    URI nestedResourceURI = Uris.create(nestedResourceURIString);

                    if (LyoProviderUtils.relativeURIsAreDisabled() && !nestedResourceURI.isAbsolute()) {
                        throw new OslcCoreRelativeURIException(beanClass, accessor.getSetter().getName(), nestedResourceURI);
                    }

                    return nestedResourceURI;
                }
            }

            // instantiate the expected type and build the resource
            else {
                Object nestedBean = type.getDeclaredConstructor().newInstance();
                buildResource(type, nestedBean, nestedResource, rdfTypes);
                return nestedBean;
            }
        }

        return null;
    }

    private Object buildAccessorLiteral(String value, Class<?> type) throws DatatypeConfigurationException {
        if (String.class == type) {
            return value;
        } else if ((Boolean.class == type) || (Boolean.TYPE == type)) {
            // XML supports both 'true' and '1' for a true Boolean.
            // Cannot use Boolean.parseBoolean since it supports case-insensitive TRUE.
            if ((Boolean.TRUE.toString().equals(value)) || ("1".equals(value))) {
                return Boolean.TRUE;
            }
            // XML supports both 'false' and '0' for a false Boolean.
            else if ((Boolean.FALSE.toString().equals(value)) || ("0".equals(value))) {
                return Boolean.FALSE;
            } else {
                throw new IllegalArgumentException("'" + value + "' has wrong format for Boolean.");
            }
        } else if ((Byte.class == type) || (Byte.TYPE == type)) {
            return Byte.valueOf(value);
        } else if ((Short.class == type) || (Short.TYPE == type)) {
            return Short.valueOf(value);
        } else if ((Integer.class == type) || (Integer.TYPE == type)) {
            return Integer.valueOf(value);
        } else if ((Long.class == type) || (Long.TYPE == type)) {
            return Long.valueOf(value);
        } else if (BigInteger.class == type) {
            return new BigInteger(value);
        } else if ((Float.class == type) || (Float.TYPE == type)) {
            return JenaProvider.dataTypes().XSD_FLOAT().parseValidated(value);
        } else if ((Double.class == type) || (Double.TYPE == type)) {
            return JenaProvider.dataTypes().XSD_DOUBLE().parseValidated(value);
        } else if (Date.class == type) {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(value).toGregorianCalendar().getTime();
        } else {
            return null;
        }
    }

    private Object buildAccessorReifiedResource(List<ReifiedStatement> reifiedStatements, Class<?> reifiedClass, Class<?> type, Object value,
            Set<String> rdfTypes) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            SecurityException, NoSuchMethodException, DatatypeConfigurationException, OslcCoreApplicationException, URISyntaxException {

        // This property supports reified statements.
        // Create the new resource to hold the value and any metadata.
        Object reifiedResource = reifiedClass.getDeclaredConstructor().newInstance();

        // Find a setter for the actual value.
        for (Method method : reifiedClass.getMethods()) {
            if ("setValue".equals(method.getName())) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if ((parameterTypes.length == 1) && parameterTypes[0].isAssignableFrom(type)) {
                    method.invoke(reifiedResource, value);
                    break;
                }
            }
        }

        for (ReifiedStatement reifiedStatement : reifiedStatements) {
            buildResource(reifiedClass, reifiedResource, reifiedStatement, rdfTypes);
        }

        return reifiedResource;
    }

    private Object buildExtendedValue(Statement statement, Class<?> beanClass, RDFNode object, QName propertyQName, Set<String> rdfTypes)
            throws URISyntaxException, IllegalArgumentException, SecurityException, DatatypeConfigurationException, IllegalAccessException,
            InstantiationException, InvocationTargetException, OslcCoreApplicationException, NoSuchMethodException {

        // literal?
        if (object.isLiteral()) {
            Literal literal = object.asLiteral();
            return buildExtendedLiteral(propertyQName, rdfTypes, literal);
        }

        // resource
        else {
            Resource nestedResource = object.asResource();
            return buildExtendedResource(statement, beanClass, rdfTypes, nestedResource);
        }
    }

    private Object buildExtendedLiteral(QName propertyQName, Set<String> rdfTypes, Literal literal)
            throws DatatypeConfigurationException, InstantiationException {

        // fix for Bug 412789
        if (ResourceShapes.inferTypeFromShape()) {

            // get property data type
            RDFDataType dataType = literal.getDatatype();

            // infer the data type from the Resource Shape only if the
            // data type was not explicit passed in the original request
            if (null == dataType) {
                Object newObject = ResourceShapes.getValueBasedOnResourceShapeType(rdfTypes, propertyQName, literal.getString());

                // return the value only if the type was really inferred
                // from the resource shape, otherwise keep the same
                // behavior, i.e., return a String value
                if (null != newObject) {
                    return newObject;
                }
            }
        }

        // obtain the underlying literal value
        Object literalValue;
        try {
            literalValue = literal.getValue();
        } catch (RuntimeException e) {
            String rawValue = literal.getString();
            String datatype = literal.getDatatypeURI();

            if ("DatatypeFormatException".equals(e.getClass().getSimpleName())) { //$NON-NLS-1$ //NOSONAR
                if ("false".equals(System.getProperty(AbstractOslcRdfXmlProvider.OSLC4J_STRICT_DATATYPES))) { // NOSONAR
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Property " + propertyQName.getNamespaceURI() + propertyQName.getLocalPart() + " could not be parsed as datatype "
                                + literal.getDatatypeURI(), e);
                    }

                    return new UnparseableLiteral(rawValue, datatype);
                }
            }

            throw e;
        }

        // date?
        if (JenaProvider.dataTypes().isXsdDateTime(literalValue)) {
            return JenaProvider.dataTypes().getXsdDateTimeAsCalendar(literalValue).getTime();
        }

        // xml literal?
        else if (JenaProvider.dataTypes().XML_LITERAL().getURI().equals(literal.getDatatypeURI())) {
            return new XMLLiteral(literal.getString());
        }

        // decimal?
        else if (JenaProvider.dataTypes().XSD_DECIMAL().getURI().equals(literal.getDatatypeURI())) {
            return new BigDecimal(literal.getString());
        }

        // anything else
        else {
            return literalValue;
        }
    }

    private Object buildExtendedResource(Statement statement, Class<?> beanClass, Set<String> rdfTypes, Resource nestedResource)
            throws DatatypeConfigurationException, IllegalAccessException, InstantiationException, InvocationTargetException,
            OslcCoreApplicationException, URISyntaxException, NoSuchMethodException {

        // inline resource?
        if ((nestedResource.getURI() == null) || nestedResource.listProperties().hasNext()) {

            // already visited?
            String resourceName = getResourceName(nestedResource);
            if (visitedResources.containsKey(resourceName)) {
                Object visitedResource = visitedResources.get(resourceName);

                // reified title?
                if (visitedResource instanceof IResource) {
                    URI visitedResourceAbout = ((IResource) visitedResource).getAbout();
                    if (visitedResourceAbout != null) {
                        Optional<String> title = getReifiedTitle(statement.listReifiedStatements());
                        if (title.isPresent()) {

                            // create a link
                            return new Link(visitedResourceAbout, title.get());
                        }
                    }
                }

                return visitedResource;
            }

            else {
                // sequence?
                if (RdfCollections.isSequenceSupported()) {
                    Resource rdfType = nestedResource.getPropertyResourceValue(JenaProvider.resources().RDF_TYPE());
                    if ((rdfType != null) && rdfType.isURIResource() && rdfType.getURI().equals(TYPE_SEQ)) {
                        return buildExtendedSequence(beanClass, rdfTypes, nestedResource);
                    }
                }

                // any resource
                AbstractResource any = new AnyResource();
                buildResource(AnyResource.class, any, nestedResource, rdfTypes);
                return any;
            }
        }

        else {
            // It's a resource reference.
            URI nestedResourceURI = Uris.create(nestedResource.getURI());
            if (LyoProviderUtils.relativeURIsAreDisabled() && !nestedResourceURI.isAbsolute()) {
                throw new OslcCoreRelativeURIException(beanClass, "<none>", nestedResourceURI);
            }

            // reified title?
            Optional<String> title = getReifiedTitle(statement.listReifiedStatements());
            if (title.isPresent()) {

                // create a link
                return new Link(nestedResourceURI, title.get());
            }

            // just return the URI if no associated title was found
            return nestedResourceURI;
        }
    }

    private static Optional<String> getReifiedTitle(Iterator<ReifiedStatement> reifiedStatements) {

        // reified statements?
        if (reifiedStatements.hasNext()) {

            // look for a title in reified statements
            while (reifiedStatements.hasNext()) {
                ReifiedStatement reifiedStatement = reifiedStatements.next();
                Statement titleStatement = reifiedStatement.getProperty(JenaProvider.resources().createProperty(PROPERTY_DCTERMS_TITLE));
                if (titleStatement != null) {

                    // create a link
                    String title = titleStatement.getObject().asLiteral().getString();
                    return Optional.ofNullable(title);
                }
            }
        }

        return Optional.empty();
    }

    private Object buildExtendedSequence(Class<?> beanClass, Set<String> rdfTypes, Resource sequenceResource)
            throws DatatypeConfigurationException, IllegalAccessException, InstantiationException, InvocationTargetException,
            OslcCoreApplicationException, URISyntaxException, NoSuchMethodException {

        // loop on properties
        Map<Object, Integer> orders = new HashMap<>();
        for (Iterator<Statement> i = sequenceResource.listProperties(); i.hasNext();) {
            Statement statement = i.next();
            Property property = statement.getPredicate();

            // "rdf:_<number>" property?
            if (property.getNameSpace().equals(OslcConstants.RDF_NAMESPACE) && property.getLocalName().startsWith("_")) {
                try {
                    // the number in the property name is the order in the sequence
                    int order = Integer.parseInt(property.getLocalName().substring(1));

                    // build the sequence member
                    QName propertyQName = new QName(property.getNameSpace(), property.getLocalName());
                    Object value = buildExtendedValue(statement, beanClass, statement.getObject(), propertyQName, rdfTypes);

                    // keep track of the value and its order
                    orders.put(value, order);
                } catch (NumberFormatException e) {
                    LOGGER.warn("Unexpected format for sequence property: {}", property.getURI(), e);
                }
            }
        }

        // build a sequence and order the values properly
        List<Object> sequence = RdfCollections.createSequence();
        sequence.addAll(orders.keySet());
        Collections.sort(sequence, (o1, o2) -> orders.get(o1).compareTo(orders.get(o2)));
        return sequence;
    }

    private void markVisited(Resource resource, Object value) {
        visitedResources.put(getResourceName(resource), value);
    }

    private static String getResourceName(Resource resource) {
        if (resource.getURI() != null) {
            return resource.getURI();
        } else {
            return resource.getId().getLabelString();
        }
    }

}
