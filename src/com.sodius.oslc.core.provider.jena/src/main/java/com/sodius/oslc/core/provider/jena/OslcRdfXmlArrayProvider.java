/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.jena;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.eclipse.lyo.oslc4j.core.annotation.OslcNotQueryResult;
import org.eclipse.lyo.oslc4j.core.model.OslcMediaType;

@Provider
@Produces({ OslcMediaType.APPLICATION_RDF_XML })
@Consumes({ OslcMediaType.APPLICATION_RDF_XML })
public class OslcRdfXmlArrayProvider extends AbstractOslcRdfXmlProvider implements MessageBodyReader<Object[]>, MessageBodyWriter<Object[]> {
    public OslcRdfXmlArrayProvider() {
        super();
    }

    @Override
    public long getSize(final Object[] objects, final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType) {
        return -1;
    }

    @Override
    public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType) {
        return (type.isArray()) && (isWriteable(type.getComponentType(), annotations, mediaType, OslcMediaType.APPLICATION_RDF_XML_TYPE,
                OslcMediaType.APPLICATION_XML_TYPE, OslcMediaType.TEXT_XML_TYPE, OslcMediaType.TEXT_TURTLE_TYPE,
                OslcMediaType.APPLICATION_JSON_LD_TYPE));
    }

    @Override
    public void writeTo(final Object[] objects, final Class<?> type, final Type genericType, final Annotation[] annotations,
            final MediaType mediaType, final MultivaluedMap<String, Object> map, final OutputStream outputStream)
            throws IOException, WebApplicationException {
        OslcNotQueryResult notQueryResult = type.getComponentType().getAnnotation(OslcNotQueryResult.class);

        writeTo((notQueryResult != null) && notQueryResult.value() ? false : true, objects, mediaType, map, outputStream); // NOSONAR
    }

    @Override
    public boolean isReadable(final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType) {
        return (type.isArray())
                && (isReadable(type.getComponentType(), mediaType, OslcMediaType.APPLICATION_RDF_XML_TYPE, OslcMediaType.APPLICATION_XML_TYPE,
                        OslcMediaType.TEXT_XML_TYPE, OslcMediaType.TEXT_TURTLE_TYPE, OslcMediaType.APPLICATION_JSON_LD_TYPE));
    }

    @Override
    public Object[] readFrom(final Class<Object[]> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType,
            final MultivaluedMap<String, String> map, final InputStream inputStream) throws IOException, WebApplicationException {
        return readFrom(type.getComponentType(), mediaType, map, inputStream);
    }
}