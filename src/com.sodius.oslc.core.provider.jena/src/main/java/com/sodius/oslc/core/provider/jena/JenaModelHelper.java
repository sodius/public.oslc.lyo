/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.jena;

import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;

import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.core.model.ResponseInfo;

import com.sodius.oslc.core.provider.jena.internal.JenaModelBuilder;
import com.sodius.oslc.core.provider.jena.internal.ResourceBuilder;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.Resource;

public final class JenaModelHelper {

    private JenaModelHelper() {
        super();
    }

    public static Model createJenaModel(Object[] objects) throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, OslcCoreApplicationException {
        return createJenaModel(null, null, null, objects, null);
    }

    public static Model createJenaModel(String descriptionAbout, String responseInfoAbout, ResponseInfo<?> responseInfo, Object[] objects,
            Map<String, Object> properties) throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, OslcCoreApplicationException {

        return JenaModelBuilder.build(descriptionAbout, responseInfoAbout, responseInfo, objects, properties);
    }

    public static Object fromJenaResource(Resource resource, Class<?> beanClass)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InstantiationException,
            InvocationTargetException, OslcCoreApplicationException, URISyntaxException, SecurityException, NoSuchMethodException {
        return ResourceBuilder.build(resource, beanClass);
    }

    public static Object[] fromJenaModel(Model model, Class<?> beanClass)
            throws DatatypeConfigurationException, IllegalAccessException, IllegalArgumentException, InstantiationException,
            InvocationTargetException, OslcCoreApplicationException, URISyntaxException, SecurityException, NoSuchMethodException {
        return ResourceBuilder.build(model, beanClass);
    }

}
