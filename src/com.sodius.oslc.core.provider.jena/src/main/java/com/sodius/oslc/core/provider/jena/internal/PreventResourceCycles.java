package com.sodius.oslc.core.provider.jena.internal;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.core.model.IExtendedResource;
import org.eclipse.lyo.oslc4j.core.model.IResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sodius.oslc.core.provider.internal.JavaResourceShape;
import com.sodius.oslc.core.provider.internal.PropertyAccessor;

/*
 * If a resource has an extended property whose value is its own URI (or one of its ancestors),
 * ResourceBuilder replaces the URI by the resource itself.
 * The Lyo writer is then unable to serialize back the resource.
 *
 * Need to make sure only the URI is used in extended properties in the deserialized resource.
 *
 * Infinite loop requesting a resource that has a link to itself
 * https://sodius.atlassian.net/browse/SECOLLAB-163
 * https://sodius.atlassian.net/browse/PRDOSLC-3
 * https://sodiuswillert.atlassian.net/browse/PRDOSLC-962
 */
class PreventResourceCycles implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreventResourceCycles.class);

    private final List<Object> objects;

    PreventResourceCycles(List<Object> objects) {
        this.objects = objects;
    }

    @Override
    public void run() {
        for (Object object : objects) {
            if (object instanceof IExtendedResource) {
                IExtendedResource resource = (IExtendedResource) object;
                run(resource, new HashSet<>());
            }
        }
    }

    private void run(IExtendedResource resource, Set<IResource> parentResources) {

        // Not yet visited?
        // Implementation note: this Set is using identity comparison, as we don't override hashCode/equals in IExtendedResource subclasses.
        // We're safe here as ResourceBuilder ensures only one instance of IExtendedResource is created per URI.
        // (see ResourceBuilder.visitedResources and ResourceBuilder.getResourceName)
        if (parentResources.add(resource)) {

            // extended properties
            if (resource.getExtendedProperties() != null) {
                for (Entry<QName, Object> property : resource.getExtendedProperties().entrySet()) {
                    runExtendedProperty(property, parentResources);
                }
            }

            // nested resources using getters
            try {
                for (Method method : getResourceMethods(resource.getClass())) {
                    Object value = method.invoke(resource);
                    runOwnedResource(value, parentResources);
                }
            } catch (OslcCoreApplicationException | ReflectiveOperationException e) {
                LOGGER.warn("Could not prevent cycles in resource", e); //$NON-NLS-1$
            }
        }
    }

    private void runExtendedProperty(Entry<QName, Object> property, Set<IResource> parentResources) {

        // resource?
        if (property.getValue() instanceof IResource) {
            IResource value = (IResource) property.getValue();
            runExtendedPropertyResource(property, value, parentResources);
        }

        // collection of resources?
        else if (property.getValue() instanceof Collection<?>) {
            Collection<?> collection = (Collection<?>) property.getValue();
            runExtendedPropertyResources(collection, parentResources);
        }
    }

    private void runExtendedPropertyResource(Entry<QName, Object> property, IResource value, Set<IResource> parentResources) {

        // the resource has a URI and is already known in its parent hierarchy?
        if ((value.getAbout() != null) && parentResources.contains(value)) {

            // permute the resource to use only its URI
            property.setValue(value.getAbout());
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void runExtendedPropertyResources(Collection collection, Set<IResource> parentResources) {
        for (Object value : new ArrayList(collection)) {
            if (value instanceof IResource) {
                IResource valueResource = (IResource) value;

                // the resource has URI and is already known it its parent hierarchy?
                if ((valueResource.getAbout() != null) && parentResources.contains(valueResource)) {

                    // permute the resource to use only its URI
                    if (collection instanceof List) {
                        ((List) collection).set(((List) collection).indexOf(valueResource), valueResource.getAbout());
                    } else {
                        collection.remove(valueResource);
                        collection.add(valueResource.getAbout());
                    }
                }
            }
        }
    }

    private void runOwnedResource(Object value, Set<IResource> parentResources) {
        if (value instanceof IExtendedResource) {
            run((IExtendedResource) value, parentResources);
        }

        else if (value instanceof IExtendedResource[]) {
            for (IExtendedResource arrayValue : (IExtendedResource[]) value) {
                run(arrayValue, parentResources);
            }
        }

        else if (value instanceof Collection) {
            for (Object collectionValue : (Collection<?>) value) {
                if (collectionValue instanceof IExtendedResource) {
                    run((IExtendedResource) collectionValue, parentResources);
                }
            }
        }
    }

    private static Collection<Method> getResourceMethods(Class<?> clazz) throws OslcCoreApplicationException {
        Collection<Method> result = new ArrayList<>();
        for (PropertyAccessor accessor : JavaResourceShape.valueOf(clazz).getAccessors()) {
            Class<?> returnType = accessor.getGetter().getReturnType();
            if (isResourceReturnType(returnType)) {
                result.add(accessor.getGetter());
            }
        }
        return result;
    }

    private static boolean isResourceReturnType(Class<?> type) {
        return IExtendedResource.class.isAssignableFrom(type) //
                || (type.isArray() && isResourceReturnType(type.getComponentType())) //
                || Collection.class.isAssignableFrom(type);
    }
}
