package com.sodius.oslc.core.provider.jena.internal;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.List;

import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.lyo.oslc4j.core.UnparseableLiteral;
import org.eclipse.lyo.oslc4j.core.annotation.OslcRdfCollectionType;
import org.eclipse.lyo.oslc4j.core.model.XMLLiteral;
import org.w3c.dom.Element;

import com.sodius.oslc.core.provider.internal.RdfCollections;
import com.sodius.oslc.core.rdf.Container;
import com.sodius.oslc.core.rdf.Literal;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.RDFDataType;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.jena.JenaProvider;

class RDFNodeFactory {

    private final Model model;

    RDFNodeFactory(Model model) {
        this.model = model;
    }

    Literal createLiteral(Calendar value, RDFDataType dataType) {
        if ((dataType != null) && (JenaProvider.dataTypes().XSD_DATE().equals(dataType))) {
            String valueDate = JenaProvider.dataTypes().formatDate(value);
            return model.createTypedLiteral(valueDate, JenaProvider.dataTypes().XSD_DATE());
        } else {
            return model.createTypedLiteral(value);
        }
    }

    Literal createLiteral(String value) {
        return model.createLiteral(value);
    }

    Literal createLiteral(UnparseableLiteral value) {
        return model.createLiteral(value.getRawValue());
    }

    Literal createLiteral(XMLLiteral value) {
        RDFDataType dataType = JenaProvider.dataTypes().XML_LITERAL();
        return model.createTypedLiteral(value.getValue(), dataType);
    }

    Literal createLiteral(Element element) {
        try {
            StreamResult result = new StreamResult(new StringWriter());
            TransformerSupplier.INSTANCE.get().transform(new DOMSource(element), result);
            String stringValue = result.getWriter().toString();
            return model.createLiteral(stringValue, true /* well formed */);
        } catch (TransformerException e) {
            throw new RuntimeException(e); // NOSONAR
        }
    }

    Literal createLiteral(Float f) {
        RDFDataType dataType = JenaProvider.dataTypes().XSD_FLOAT();
        if (f.compareTo(Float.POSITIVE_INFINITY) == 0) {
            return model.createTypedLiteral("INF", dataType);
        } else if (f.compareTo(Float.NEGATIVE_INFINITY) == 0) {
            return model.createTypedLiteral("-INF", dataType);
        }

        return model.createTypedLiteral(f, dataType);
    }

    Literal createLiteral(Double d) {
        RDFDataType dataType = JenaProvider.dataTypes().XSD_DOUBLE();
        if (d.compareTo(Double.POSITIVE_INFINITY) == 0) {
            return model.createTypedLiteral("INF", dataType);
        } else if (d.compareTo(Double.NEGATIVE_INFINITY) == 0) {
            return model.createTypedLiteral("-INF", dataType);
        }

        return model.createTypedLiteral(d, dataType);
    }

    RDFNode createContainer(OslcRdfCollectionType collectionType, List<RDFNode> nodes) {
        if (RdfCollections.RDF_LIST.equals(collectionType.collectionType())) {
            return model.createList(nodes.iterator());
        }

        Container container;

        if (RdfCollections.RDF_ALT.equals(collectionType.collectionType())) {
            container = model.createAlt();
        } else if (RdfCollections.RDF_BAG.equals(collectionType.collectionType())) {
            container = model.createBag();
        } else {
            // Bad performance when writing large rdf:Seq
            // let's create the Seq on our own
            Resource seq = model.createResource();
            seq.addProperty(JenaProvider.resources().RDF_TYPE(), JenaProvider.resources().RDF_SEQ());
            int size = 0;
            for (RDFNode node : nodes) {
                seq.addProperty(JenaProvider.resources().RDF_LI(++size), node);
            }
            return seq.asSeq();
        }

        for (RDFNode node : nodes) {
            container.add(node);
        }

        return container;
    }

}
