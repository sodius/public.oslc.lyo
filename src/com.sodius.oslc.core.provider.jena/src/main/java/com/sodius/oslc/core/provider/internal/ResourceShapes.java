/*
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;

import org.eclipse.lyo.oslc4j.core.OSLC4JConstants;
import org.eclipse.lyo.oslc4j.core.model.ResourceShape;
import org.eclipse.lyo.oslc4j.core.model.XMLLiteral;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sodius.oslc.core.provider.jena.JenaIO;
import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.RDFDataType;
import com.sodius.oslc.core.rdf.jena.JenaProvider;

/*
 * Extracted from org.eclipse.lyo.oslc4j.core.OSLC4JUtils,
 * to remove dependency on org.apache.jena.ext.com.google.common.base.Strings,
 * which is no longer provided by Jena 4.9
 */
public class ResourceShapes {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceShapes.class);

    /**
     * This method receives the property name and the property value and tries to infer the property
     * type from the pre-defined list of Resource Shapes. Then returns the corresponding java object
     * for the given object value. Returns a null object when it was not possible to infer the
     * property type from the list of Resource Shapes.
     *
     * @param propertyQName
     *            Property information
     * @param originalValue
     *            Property value
     *
     * @return Java object related to the Resource Shape type.
     */
    @SuppressWarnings("java:S3776") // legacy complex method that we don't want to refactor
    public static Object getValueBasedOnResourceShapeType(Set<String> rdfTypesList, QName propertyQName, Object originalValue)
            throws DatatypeConfigurationException, IllegalArgumentException, InstantiationException {
        if ((null == rdfTypesList) || rdfTypesList.isEmpty() || (null == propertyQName) || (null == originalValue)) {
            return null;
        }
        try {
            // get the pre-defined list of ResourceShapes
            List<ResourceShape> shapes = JenaIO.getResourceShapes();

            if (shapes.isEmpty()) {
                return null;
            }

            // try to find the attribute type in the list of
            // resource shapes
            String propertyName = propertyQName.getNamespaceURI() + propertyQName.getLocalPart();

            for (ResourceShape shape : shapes) {

                // ensure that the current resource shape matches the resource rdf:type
                if (!doesResourceShapeMatchRdfTypes(shape, rdfTypesList)) {
                    continue;
                }

                org.eclipse.lyo.oslc4j.core.model.Property[] props = shape.getProperties();

                for (org.eclipse.lyo.oslc4j.core.model.Property prop : props) { // NOSONAR don't want to extract into another method
                    URI propDefinition = prop.getPropertyDefinition();

                    if (!propertyName.equals(propDefinition.toString())) {
                        continue;
                    }
                    URI propValueType = prop.getValueType();

                    if (null == propValueType) {
                        continue;
                    }

                    RDFDataType dataTypeFromShape = JenaProvider.dataTypes().getTypeByName(propValueType.toString());

                    // this is a literal type
                    if (null == dataTypeFromShape) {
                        continue;
                    }
                    try { // NOSONAR don't want to extract into another method

                        // special treatment for XMLLiteral
                        if (isXmlLiteralProperty(propValueType)) {
                            return xmlLiteralPropertyFrom(originalValue);
                        }

                        // special treatment for Date
                        if (isDateProperty(dataTypeFromShape)) {
                            return datePropertyFrom(originalValue);
                        }

                        // special treatment for Boolean
                        if (isBooleanProperty(dataTypeFromShape)) {
                            return booleanPropertyFrom(originalValue);

                        }

                        // special treatment for double
                        if (isDoubleProperty(dataTypeFromShape)) {
                            return doublePropertyFrom(originalValue);
                        }

                        // special treatment for float
                        if (isFloatProperty(dataTypeFromShape)) {
                            return floatPropertyFrom(originalValue);
                        }
                        Constructor<?> cons = dataTypeFromShape.getJavaClass().getConstructor(String.class);
                        return cons.newInstance(originalValue.toString());
                    } catch (RuntimeException | InvocationTargetException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            }
        } catch (NoSuchMethodException | IllegalAccessException e) {
            // if there is any error while creating the new object, return null,
            // i.e use the original value and not the new one.
            LOGGER.warn("Could not create extended value <{}> based on shape", propertyQName, e);
            return null;
        }

        return null;
    }

    /**
     * Returns the boolean value of the system property {@code org.eclipse.lyo.oslc4j.inferTypeFromResourceShape}.
     * Default is {@code false} if not set.
     * This value determines whether Resource Shapes registered on {@link JenaIO#getResourceShapes()} are used when parsing RDF content
     * to infer the property Data Types.
     */
    /*
     * See https://bugs.eclipse.org/bugs/show_bug.cgi?id=412789
     */
    public static boolean inferTypeFromShape() {
        return Boolean.getBoolean(OSLC4JConstants.OSLC4J_INFER_TYPE_FROM_SHAPE);
    }

    /**
     * This method returns true if the given Resource Shape describes array
     * matches the list of RDF types.
     *
     * @param shape
     *            Resource Shape
     * @param rdfTypesList
     *            List of rdf:types
     *
     * @return True if the ResourceShape type is in the list of rdf:types,
     *         otherwise returns false.
     */
    private static boolean doesResourceShapeMatchRdfTypes(ResourceShape shape, Set<String> rdfTypesList) {
        if (null != shape) {
            URI[] describes = shape.getDescribes();
            for (URI describeUri : describes) {
                String describeUriStr = describeUri.toASCIIString();
                if (rdfTypesList.contains(describeUriStr)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static Object floatPropertyFrom(Object originalValue) {
        return JenaProvider.dataTypes().XSD_FLOAT().parseValidated(originalValue.toString());
    }

    private static Object doublePropertyFrom(Object originalValue) {
        return JenaProvider.dataTypes().XSD_DOUBLE().parseValidated(originalValue.toString());
    }

    private static Object booleanPropertyFrom(Object originalValue) {
        // XML supports both 'true' and '1' for a true Boolean.
        // Cannot use Boolean.parseBoolean since it supports case-insensitive TRUE.
        if ((Boolean.TRUE.toString().equals(originalValue.toString())) || ("1".equals(originalValue.toString()))) {
            return Boolean.TRUE;
        }
        // XML supports both 'false' and '0' for a false Boolean.
        else if ((Boolean.FALSE.toString().equals(originalValue.toString())) || ("0".equals(originalValue.toString()))) {
            return Boolean.FALSE;
        } else {
            throw new IllegalArgumentException("'" + originalValue.toString() + "' has wrong format for Boolean.");
        }
    }

    private static Object datePropertyFrom(Object originalValue) throws DatatypeConfigurationException {
        String dateStr = originalValue.toString();
        Calendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateStr).toGregorianCalendar();
        return JenaProvider.dataTypes().getXsdDateTimeFromCalendar(calendar).getTime();
    }

    private static XMLLiteral xmlLiteralPropertyFrom(Object originalValue) {
        return new XMLLiteral(originalValue.toString());
    }

    private static boolean isFloatProperty(RDFDataType dataTypeFromShape) {
        return dataTypeFromShape.getJavaClass().getCanonicalName().equals(Float.class.getCanonicalName());
    }

    private static boolean isDoubleProperty(RDFDataType dataTypeFromShape) {
        return dataTypeFromShape.getJavaClass().getCanonicalName().equals(Double.class.getCanonicalName());
    }

    private static boolean isBooleanProperty(RDFDataType dataTypeFromShape) {
        return dataTypeFromShape.getJavaClass().getCanonicalName().equals(Boolean.class.getCanonicalName());
    }

    private static boolean isDateProperty(RDFDataType dataTypeFromShape) {
        String dateTimeCanonicalName = JenaProvider.dataTypes().XSD_DATE_TIME().getJavaClass().getCanonicalName();
        return dataTypeFromShape.getJavaClass().getCanonicalName().equals(dateTimeCanonicalName);
    }

    private static boolean isXmlLiteralProperty(URI propValueType) {
        return JenaProvider.dataTypes().XML_LITERAL().getURI().equals(propValueType.toString());
    }

    /**
     * This method receives the property name and the property value and tries
     * to infer the property Data Type from the pre-defined list of Resource Shapes.
     * Returns a null object when it was not possible to infer the property Data Type
     * from the list of Resource Shapes.
     *
     * @param rdfTypesList
     *            List of rdf:types
     * @param property
     *            Property information
     * @return Java object related to the Resource Shape type.
     */
    @SuppressWarnings("java:S3776") // legacy complex method that we don't want to refactor
    public static RDFDataType getDataTypeBasedOnResourceShapeType(Set<String> rdfTypesList, Property property) {
        if ((null != rdfTypesList) && !rdfTypesList.isEmpty() && (null != property)) {
            try {
                // get the pre-defined list of ResourceShapes
                List<ResourceShape> shapes = JenaIO.getResourceShapes();
                if (!shapes.isEmpty()) {

                    // try to find the attribute type in the list of
                    // resource shapes
                    String propertyName = property.getURI();

                    for (ResourceShape shape : shapes) {

                        // ensure that the current resource shape matches the resource rdf:type
                        if (doesResourceShapeMatchRdfTypes(shape, rdfTypesList)) {

                            org.eclipse.lyo.oslc4j.core.model.Property[] props = shape.getProperties();

                            for (org.eclipse.lyo.oslc4j.core.model.Property prop : props) {
                                URI propDefinition = prop.getPropertyDefinition();

                                if (propertyName.equals(propDefinition.toString())) {
                                    URI propValueType = prop.getValueType();

                                    if (null == propValueType) {
                                        continue;
                                    }
                                    return JenaProvider.dataTypes().getTypeByName(propValueType.toString());
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                // if there is any error, return null
                LOGGER.warn("Could not find Data Type <{}> based on shape", property, e);
                return null;
            }
        }
        return null;
    }

    private ResourceShapes() {
    }
}
