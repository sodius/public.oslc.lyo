/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.provider.jena;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.lyo.oslc4j.core.model.ResourceShape;

import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.RDFLanguages;
import com.sodius.oslc.core.rdf.RDFReader;
import com.sodius.oslc.core.rdf.RDFWriter;
import com.sodius.oslc.core.rdf.jena.JenaProvider;

public final class JenaIO {
    private static final ErrorHandler ERROR_HANDLER = new ErrorHandler();

    /**
     * List of available ResourceShapes. This list will be used to infer the
     * property type from the resource shape and it will only be considered if
     * the property inferTypeFromShape is set to true. This is part of the the
     * fix for defect 412789.
     */
    private static final List<ResourceShape> RESOURCE_SHAPES = new ArrayList<>();

    /**
     * Returns a modifiable list of Resource Shapes to be used when inferring a property type from the Resource Shape.
     * This method should only be used when the system property {@code org.eclipse.lyo.oslc4j.inferTypeFromResourceShape} is set to {@code true}.
     *
     * @return a modifiable list of Resource Shapes
     */
    public static List<ResourceShape> getResourceShapes() {
        return RESOURCE_SHAPES;
    }

    public static Model read(InputStream input, Optional<String> language) {
        Model model = JenaProvider.models().create();

        String serializationLanguage = language.orElse("");
        RDFReader reader = getRdfReader(serializationLanguage, model);

        // Pass the empty string as the base URI.
        // This allows Jena to resolve relative URIs commonly used to in reified statements for OSLC link labels.
        // See this section of the CM specification for an example:
        // http://open-services.net/bin/view/Main/CmSpecificationV2?sortcol=table;up=#Labels_for_Relationships
        String base = ""; //$NON-NLS-1$

        reader.read(model, input, base);

        return model;
    }

    public static void write(Model model, OutputStream outputStream, Optional<String> language) {
        String serializationLanguage = language.orElse("");
        RDFWriter writer = getRdfWriter(serializationLanguage, model);
        try {
            if (serializationLanguage.equals(RDFLanguages.XML) || serializationLanguage.equals(RDFLanguages.XML_ABBREV)) {
                // XML (and Jena) default to UTF-8, but many libs default to ASCII, so need
                // to set this explicitly
                writer.setProperty("showXmlDeclaration", "false");
                String xmlDeclaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

                outputStream.write(xmlDeclaration.getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            throw JenaProvider.models().wrapException(e);
        }

        writer.write(model, outputStream, null);
    }

    private static RDFWriter getRdfWriter(final String serializationLanguage, final Model model) {
        RDFWriter writer = null;
        if (serializationLanguage.isEmpty()) {
            writer = model.getWriter();
        } else if (serializationLanguage.equals(RDFLanguages.XML_ABBREV)) {
            writer = new RdfXmlAbbreviatedWriter();
        } else {
            writer = model.getWriter(serializationLanguage);
        }
        writer.setProperty("showXmlDeclaration", "false");
        writer.setErrorHandler(ERROR_HANDLER);
        return writer;
    }

    private static RDFReader getRdfReader(final String serializationLanguage, final Model model) {
        RDFReader reader = null;
        if ((serializationLanguage.isEmpty()) || serializationLanguage.equals(RDFLanguages.XML_ABBREV)) {
            reader = model.getReader(); // Default reader handles both xml and abbreviated xml
        } else {
            reader = model.getReader(serializationLanguage);
        }
        reader.setErrorHandler(ERROR_HANDLER);
        return reader;
    }

    private JenaIO() {
    }

}
