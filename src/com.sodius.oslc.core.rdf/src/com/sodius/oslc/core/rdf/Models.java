/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf;

import java.io.OutputStream;
import java.io.Writer;

/**
 * Obtain a instance of <code>Model</code> with <code>com.sodius.oslc.core.rdf.jena.JenaProvider.models()</code>
 */
public interface Models {

    public Model create();

    public RuntimeException wrapException(Exception exception);

    public Writer asUTF8Writer(OutputStream out);

    public String resolveCharset(String name);

    public String substituteEntitiesInElementContent(String s);

}
