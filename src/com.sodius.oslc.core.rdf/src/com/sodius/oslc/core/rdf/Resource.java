/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf;

public interface Resource extends RDFNode {

    public String getURI();

    public AnonId getId();

    public String getNameSpace();

    public String getLocalName();

    public boolean hasProperty(Property property);

    public boolean hasProperty(Property property, RDFNode node);

    public void addProperty(Property property, RDFNode node);

    public void addProperty(Property property, String value);

    public Statement getProperty(Property property);

    public Resource getPropertyResourceValue(Property property);

    public ExtendedIterator<Statement> listProperties();

    public ExtendedIterator<Statement> listProperties(Property property);

    public Resource removeProperties();

    /**
     * @since 3.1.0
     */
    public Resource removeAll(Property property);
}
