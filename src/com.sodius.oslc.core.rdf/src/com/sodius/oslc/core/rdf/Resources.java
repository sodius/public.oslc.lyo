/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf;

/**
 * Obtain a instance of <code>Model</code> with <code>com.sodius.oslc.core.rdf.jena.JenaProvider.resources()</code>
 */
@SuppressWarnings("java:S100") // Cannot rename the methods
public interface Resources {

    public Property RDF_TYPE();

    public Property RDF_FIRST();

    public Resource RDF_NIL();

    public Property RDF_LI(int i);

    public Property RDF_OBJECT();

    public Property RDF_PREDICATE();

    public Property RDF_REST();

    public Resource RDF_SEQ();

    public Resource RDF_STATEMENT();

    public Resource RDF_SUBJECT();

    public Resource RDFS_CONTAINER();

    public Property RDFS_MEMBER();

    public Resource createResource(String uri);

    public Property createProperty(String uri);

}
