/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf;

import java.util.Calendar;

/**
 * Obtain a instance of <code>Model</code> with <code>com.sodius.oslc.core.rdf.jena.JenaProvider.dataTypes()</code>
 */
@SuppressWarnings("java:S100") // Cannot rename the methods
public interface RDFDataTypes {

    public RDFDataType XML_LITERAL();

    public RDFDataType XSD_DATE();

    public RDFDataType XSD_DATE_TIME();

    public RDFDataType XSD_DECIMAL();

    public RDFDataType XSD_DOUBLE();

    public RDFDataType XSD_FLOAT();

    public String formatDate(Calendar calendar);

    public boolean isXsdDateTime(Object value);

    public Calendar getXsdDateTimeAsCalendar(Object value);

    public Calendar getXsdDateTimeFromCalendar(Calendar calendar);

    public RDFDataType getTypeByName(String uri);

}
