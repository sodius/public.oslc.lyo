/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf;

public final class RDFLanguages {

    public static final String XML = "RDF/XML";
    public static final String XML_ABBREV = "RDF/XML-ABBREV";
    public static final String TURTLE = "TURTLE";
    public static final String JSONLD = "JSON-LD";

    private RDFLanguages() {
    }
}
