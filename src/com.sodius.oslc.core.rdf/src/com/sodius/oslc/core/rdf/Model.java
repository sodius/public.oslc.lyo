/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf;

import java.util.Iterator;
import java.util.Map;
import java.util.function.Predicate;

/**
 * @see Models#create()
 * @see Models#read(java.io.InputStream, String, java.util.Optional)
 */
public interface Model {

    public Map<String, String> getNsPrefixMap();

    public String getNsURIPrefix(String uri);

    public void setNsPrefix(String prefix, String uri);

    public Container createAlt();

    public Container createBag();

    public Resource createList(Iterator<? extends RDFNode> members);

    public Container createSeq();

    public Resource createResource();

    public Resource createResource(String uri);

    public Resource createResource(String uri, Resource type);

    public Property createProperty(String uri);

    public Property createProperty(String nameSpace, String localName);

    public Resource createResource(Resource type);

    public Literal createLiteral(String value);

    public Literal createLiteral(String value, boolean wellFormed);

    public Literal createTypedLiteral(Object value);

    public Literal createTypedLiteral(int value);

    public Literal createTypedLiteral(String lexicalForm, RDFDataType datatype);

    public Literal createTypedLiteral(Object value, RDFDataType datatype);

    public Literal createTypedLiteral(String lexicalForm, String typeURI);

    public Statement createStatement(Resource subject, Property property, RDFNode object);

    public boolean contains(Statement statement);

    public void add(Statement statement);

    public void remove(Statement statement);

    public Resource getResource(String uri);

    public Property getProperty(String uri);

    public ExtendedIterator<Resource> listSubjectsWithProperty(Property property);

    public ExtendedIterator<Resource> listSubjectsWithProperty(Property property, RDFNode object);

    public ExtendedIterator<Statement> listStatements();

    public ExtendedIterator<Statement> listStatements(Resource subject, Property property, RDFNode object);

    public ExtendedIterator<Statement> listStatements(Predicate<Statement> statement);

    public ExtendedIterator<ReifiedStatement> listReifiedStatements();

    public ExtendedIterator<ReifiedStatement> listReifiedStatements(Statement statement);

    public boolean isIsomorphicWith(Model model);

    public RDFReader getReader();

    public RDFReader getReader(String lang);

    public RDFWriter getWriter();

    public RDFWriter getWriter(String lang);
}
