/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import java.util.Objects;

class JenaObject<T> {

    private final T delegate;

    JenaObject(T delegate) {
        this.delegate = delegate;
    }

    final T getJenaObject() {
        return delegate;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(delegate);
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
        JenaObject<?> other = (JenaObject<?>) obj;
        return Objects.equals(delegate, other.delegate);
    }

    @Override
    public final String toString() {
        return delegate.toString();
    }

    public static <T> T getJenaObject(JenaObject<T> jenaObject) {
        if (null == jenaObject) {
            return null;
        }
        return jenaObject.getJenaObject();
    }

}
