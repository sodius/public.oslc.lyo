/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import com.sodius.oslc.core.rdf.AnonId;
import com.sodius.oslc.core.rdf.ExtendedIterator;
import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Statement;

class ResourceImpl<T extends org.apache.jena.rdf.model.Resource> extends RDFNodeImpl<T> implements Resource {

    ResourceImpl(T resource) {
        super(resource);
    }

    @Override
    public final String getURI() {
        return getJenaObject().getURI();
    }

    @Override
    public AnonId getId() {
        return JenaObjects.anonId(getJenaObject().getId());
    }

    @Override
    public String getNameSpace() {
        return getJenaObject().getNameSpace();
    }

    @Override
    public String getLocalName() {
        return getJenaObject().getLocalName();
    }

    @Override
    public final boolean hasProperty(Property property) {
        // @formatter:off
        return getJenaObject().hasProperty(
                getJenaObject((PropertyImpl) property)
        );
        // @formatter:on
    }

    @Override
    public final boolean hasProperty(Property property, RDFNode node) {
        // @formatter:off
        return getJenaObject().hasProperty(
                getJenaObject((PropertyImpl) property),
                (org.apache.jena.rdf.model.RDFNode) getJenaObject((JenaObject<?>) node)
        );
        // @formatter:on
    }

    @Override
    public final void addProperty(Property property, RDFNode node) {
        // @formatter:off
        getJenaObject().addProperty(
                getJenaObject((PropertyImpl) property),
                (org.apache.jena.rdf.model.RDFNode) getJenaObject((JenaObject<?>) node)
        );
        // @formatter:on
    }

    @Override
    public final void addProperty(Property property, String value) {
        // @formatter:off
        getJenaObject().addProperty(
                getJenaObject((PropertyImpl) property),
                value
        );
        // @formatter:on
    }

    @Override
    public final Statement getProperty(Property property) {
        // @formatter:off
        return JenaObjects.statement(getJenaObject().getProperty(
                getJenaObject((PropertyImpl) property)
        ));
        // @formatter:on
    }

    @Override
    public final Resource getPropertyResourceValue(Property property) {
        // @formatter:off
        return JenaObjects.resource(getJenaObject().getPropertyResourceValue(
                getJenaObject((PropertyImpl) property)
        ));
        // @formatter:on
    }

    @Override
    public final ExtendedIterator<Statement> listProperties() {
        return new ExtendedIteratorImpl<>(getJenaObject().listProperties());
    }

    @Override
    public final ExtendedIterator<Statement> listProperties(Property property) {
        // @formatter:off
        return new ExtendedIteratorImpl<>(getJenaObject().listProperties(
                getJenaObject((PropertyImpl) property)
        ));
        // @formatter:on
    }

    @Override
    public Resource removeProperties() {
        getJenaObject().removeProperties();
        return this;
    }

    @Override
    public Resource removeAll(Property property) {
        getJenaObject().removeAll(((PropertyImpl) property).getJenaObject());
        return this;
    }
}
