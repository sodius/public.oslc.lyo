/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import org.apache.jena.rdf.model.RDFList;

import com.sodius.oslc.core.rdf.ExtendedIterator;
import com.sodius.oslc.core.rdf.Literal;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.Resource;

abstract class RDFNodeImpl<T extends org.apache.jena.rdf.model.RDFNode> extends JenaObject<T> implements RDFNode {

    RDFNodeImpl(T resource) {
        super(resource);
    }

    @Override
    public Model getModel() {
        return JenaObjects.model(getJenaObject().getModel());
    }

    @Override
    public boolean isResource() {
        return getJenaObject().isResource();
    }

    @Override
    public boolean isAnon() {
        return getJenaObject().isAnon();
    }

    @Override
    public final boolean isURIResource() {
        return getJenaObject().isURIResource();
    }

    @Override
    public Resource asResource() {
        return JenaObjects.resource(getJenaObject().asResource());
    }

    @Override
    public boolean isLiteral() {
        return getJenaObject().isLiteral();
    }

    @Override
    public Literal asLiteral() {
        return JenaObjects.literal(getJenaObject().asLiteral());
    }

    @Override
    public boolean canAsList() {
        return getJenaObject().canAs(RDFList.class);
    }

    @SuppressWarnings({ "deprecation", "java:S1874" }) // ReifiedStatement is deprecated but still to be used (PRDOSLC-1165)
    @Override
    public boolean canAsReifiedStatement() {
        return getJenaObject().canAs(org.apache.jena.rdf.model.ReifiedStatement.class);
    }

    @Override
    public Resource asSeq() {
        return JenaObjects.container(getJenaObject().as(org.apache.jena.rdf.model.Seq.class));
    }

    @Override
    public ExtendedIterator<RDFNode> asSeqMembers() {
        return new ExtendedIteratorImpl<>(getJenaObject().as(org.apache.jena.rdf.model.Seq.class).iterator());
    }
}
