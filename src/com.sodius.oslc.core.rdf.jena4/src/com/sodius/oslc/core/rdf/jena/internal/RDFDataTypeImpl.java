/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import org.apache.jena.datatypes.xsd.XSDDatatype;

import com.sodius.oslc.core.rdf.RDFDataType;

class RDFDataTypeImpl extends JenaObject<org.apache.jena.datatypes.RDFDatatype> implements RDFDataType {

    RDFDataTypeImpl(org.apache.jena.datatypes.RDFDatatype datatype) {
        super(datatype);
    }

    @Override
    public String getURI() {
        return getJenaObject().getURI();
    }

    @Override
    public Class<?> getJavaClass() {
        return getJenaObject().getJavaClass();
    }

    @Override
    public Object parseValidated(String lexical) {
        if (getJenaObject() instanceof XSDDatatype) {
            return ((XSDDatatype) getJenaObject()).parseValidated(lexical);
        } else {
            throw new IllegalStateException("parseValidated() is only applicable to XSD data types"); //$NON-NLS-1$
        }
    }
}
