/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.impl.Util;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFWriterRegistry;
import org.apache.jena.riot.RIOT;
import org.apache.jena.riot.lang.ReaderRIOTRDFXML;
import org.apache.jena.shared.WrappedException;
import org.apache.jena.shared.WrappedIOException;
import org.apache.jena.sparql.util.Context;
import org.apache.jena.sparql.util.Symbol;
import org.apache.jena.util.CharEncoding;
import org.apache.jena.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.Models;

public class ModelsImpl implements Models {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModelsImpl.class);

    public ModelsImpl() {
        // Jena now translates RDF/XML to what they call "pretty", which is RDF/XML Abbreviated.
        // We need what they call "plain", which outputs "<rdf:Description>" nodes.
        // https://stackoverflow.com/questions/53439861/jena-model-converts-my-rdf-type-explicit-declaration-to-implicit-and-messes-with
        RDFWriterRegistry.register(Lang.RDFXML, RDFFormat.RDFXML_PLAIN);

        disableJenaErrorForSpacesInURLs();
        disableLegacyJenaErrorForSpacesInURLs();
    }

    /*
     * Jena 3 is configured to reject URLs having spaces.
     * This makes Jena to reject reading RMM Compact responses.
     * Need to change this setting, so that Jena doesn't check for spaces on its own
     * and let then our ExtendedJenaModelHelper class to workaround this.
     * https://sodius.atlassian.net/browse/PRDOSLC-250
     */
    @SuppressWarnings("java:S3011") // The security bypass is necessary
    private static void disableJenaErrorForSpacesInURLs() {
        try {
            // https://github.com/apache/jena/blob/jena-3.4.0/jena-arq/src/main/java/org/apache/jena/riot/lang/ReaderRIOTRDFXML.java#L109
            Field field = ReaderRIOTRDFXML.class.getDeclaredField("errorForSpaceInURI"); //$NON-NLS-1$
            field.setAccessible(true);
            field.set(null, Boolean.FALSE);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            String message = "Failed to disable Jena error processing for spaces in URLs"; //$NON-NLS-1$
            LOGGER.error(message, e);
        }
    }

    /*
     * Jena 4.8 and higher defines a ReaderRIOTRDFXML0 highly similar to ReaderRIOTRDFXML,
     * for legacy RDF+XML parsing (parsing behavior of Jena4.7 and lower).
     * Needs to also change the errorForSpaceInURI field in that class
     */
    @SuppressWarnings("java:S3011") // The security bypass is necessary
    private static void disableLegacyJenaErrorForSpacesInURLs() {
        try {
            // https://github.com/apache/jena/blob/jena-4.8.0/jena-arq/src/main/java/org/apache/jena/riot/lang/ReaderRIOTRDFXML0.java#L101
            Class<?> readerClass = ModelsImpl.class.getClassLoader().loadClass("org.apache.jena.riot.lang.ReaderRIOTRDFXML0");
            Field field = readerClass.getDeclaredField("errorForSpaceInURI"); //$NON-NLS-1$
            field.setAccessible(true);
            field.set(null, Boolean.FALSE);

            // at this stage we know Jena 4.8 or higher is used, let's ask to use the legacy reader
            useLegacyRdfXmlReader();
        } catch (ClassNotFoundException e) {
            // expected exception when Jena 4.7 or lower is used
        } catch (NoSuchFieldException | IllegalAccessException e) {
            String message = "Failed to disable legacy Jena error processing for spaces in URLs"; //$NON-NLS-1$
            LOGGER.error(message, e);
        }
    }

    @SuppressWarnings("java:S3011") // The security bypass is necessary
    private static void useLegacyRdfXmlReader() {
        try {
            // https://github.com/apache/jena/blob/jena-3.4.0/jena-arq/src/main/java/org/apache/jena/riot/lang/ReaderRIOTRDFXML.java#L109
            Field field = RIOT.class.getDeclaredField("symRDFXML0"); //$NON-NLS-1$
            field.setAccessible(true);
            Symbol symbol = (Symbol) field.get(null);

            // call RIOT.getContext().setTrue(symbol)
            // use reflect to call the method, as I get NoSuchMethodError with a direct call without understanding why!
            Method setTrueMethod = Context.class.getDeclaredMethod("setTrue", Symbol.class); //$NON-NLS-1$
            setTrueMethod.invoke(RIOT.getContext(), symbol);
        } catch (ReflectiveOperationException e) {
            String message = "Failed to request using Jena legacy RDF/XML reader"; //$NON-NLS-1$
            LOGGER.error(message, e);
        }
    }

    @Override
    public Model create() {
        return JenaObjects.model(ModelFactory.createDefaultModel());
    }

    @Override
    public RuntimeException wrapException(Exception exception) {
        if (exception instanceof IOException) {
            throw new WrappedIOException((IOException) exception);
        } else {
            throw new WrappedException(exception);
        }
    }

    @Override
    public Writer asUTF8Writer(OutputStream out) {
        return FileUtils.asUTF8(out);
    }

    @Override
    public String resolveCharset(String name) {
        return CharEncoding.create(name).name();
    }

    @Override
    public String substituteEntitiesInElementContent(String s) {
        return Util.substituteEntitiesInElementContent(s);
    }

}
