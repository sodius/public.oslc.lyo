/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import com.sodius.oslc.core.rdf.ExtendedIterator;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.ReifiedStatement;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Statement;

class StatementImpl extends JenaObject<org.apache.jena.rdf.model.Statement> implements Statement {

    StatementImpl(org.apache.jena.rdf.model.Statement statement) {
        super(statement);
    }

    @Override
    public boolean isReified() {
        return getJenaObject().isReified();
    }

    @Override
    public Resource getSubject() {
        return JenaObjects.resource(getJenaObject().getSubject());
    }

    @Override
    public Property getPredicate() {
        return JenaObjects.property(getJenaObject().getPredicate());
    }

    @Override
    public RDFNode getObject() {
        return (RDFNode) JenaObjects.of(getJenaObject().getObject());
    }

    @Override
    public Resource getResource() {
        return JenaObjects.resource(getJenaObject().getResource());
    }

    @Override
    public Model getModel() {
        return JenaObjects.model(getJenaObject().getModel());
    }

    @SuppressWarnings({ "deprecation", "java:S1874" }) // ReifiedStatement is deprecated but still to be used (PRDOSLC-1165)
    @Override
    public ReifiedStatement createReifiedStatement() {
        return JenaObjects.reifiedStatement(getJenaObject().createReifiedStatement());
    }

    @SuppressWarnings({ "deprecation", "java:S1874" }) // ReifiedStatement is deprecated but still to be used (PRDOSLC-1165)
    @Override
    public ExtendedIterator<ReifiedStatement> listReifiedStatements() {
        return new ExtendedIteratorImpl<>(getJenaObject().listReifiedStatements());
    }
}
