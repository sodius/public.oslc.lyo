/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.sodius.oslc.core.rdf.ExtendedIterator;

class ExtendedIteratorImpl<E, T> extends JenaObject<org.apache.jena.util.iterator.ExtendedIterator<T>> implements ExtendedIterator<E> {

    public ExtendedIteratorImpl(org.apache.jena.util.iterator.ExtendedIterator<T> iterator) {
        super(iterator);
    }

    @Override
    public boolean hasNext() {
        return getJenaObject().hasNext();
    }

    @SuppressWarnings("unchecked")
    @Override
    public E next() {
        return (E) JenaObjects.of(getJenaObject().next());
    }

    @Override
    public void remove() {
        getJenaObject().remove();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<E> toList() {
        List<E> result = new ArrayList<>();
        for (Object object : getJenaObject().toList()) {
            result.add((E) JenaObjects.of(object));
        }
        return result;
    }

    @Override
    public <U> ExtendedIterator<U> mapWith(Function<E, U> function) {
        return new ExtendedIteratorImpl<>(getJenaObject().mapWith(new Function<T, U>() {
            @SuppressWarnings("unchecked")
            @Override
            public U apply(T object) {
                return function.apply((E) JenaObjects.of(object));
            }
        }));
    }

}
