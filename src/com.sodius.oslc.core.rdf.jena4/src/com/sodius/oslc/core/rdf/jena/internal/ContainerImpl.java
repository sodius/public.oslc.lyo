/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import com.sodius.oslc.core.rdf.Container;
import com.sodius.oslc.core.rdf.RDFNode;

class ContainerImpl extends ResourceImpl<org.apache.jena.rdf.model.Container> implements Container {

    ContainerImpl(org.apache.jena.rdf.model.Container resource) {
        super(resource);
    }

    @Override
    public void add(RDFNode node) {
        getJenaObject().add((org.apache.jena.rdf.model.RDFNode) getJenaObject((JenaObject<?>) node));
    }
}
