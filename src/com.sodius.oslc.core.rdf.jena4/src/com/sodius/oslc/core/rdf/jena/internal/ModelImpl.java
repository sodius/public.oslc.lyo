/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import java.util.Iterator;
import java.util.Map;
import java.util.function.Predicate;

import org.apache.jena.rdf.model.RDFReaderF;
import org.apache.jena.rdf.model.RDFWriterF;
import org.apache.jena.shared.PrefixMapping;

import com.sodius.oslc.core.rdf.Container;
import com.sodius.oslc.core.rdf.ExtendedIterator;
import com.sodius.oslc.core.rdf.Literal;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.RDFDataType;
import com.sodius.oslc.core.rdf.RDFNode;
import com.sodius.oslc.core.rdf.RDFReader;
import com.sodius.oslc.core.rdf.RDFWriter;
import com.sodius.oslc.core.rdf.ReifiedStatement;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Statement;

class ModelImpl extends JenaObject<org.apache.jena.rdf.model.Model> implements Model {

    ModelImpl(org.apache.jena.rdf.model.Model model) {
        super(model);
    }

    @Override
    public Map<String, String> getNsPrefixMap() {
        return getJenaObject().getNsPrefixMap();
    }

    @Override
    public String getNsURIPrefix(String uri) {
        return getJenaObject().getNsURIPrefix(uri);
    }

    @Override
    public void setNsPrefix(String prefix, String uri) {
        // Jena 3.17 Model class overrides PrefixMapping.setNsPrefix().
        // This wasn't the case with Jena 3.4, and that is a binary incompatible method then.
        // Force referencing PrefixMapping to restore binary compatibility.
        PrefixMapping jenaObject = getJenaObject();
        jenaObject.setNsPrefix(prefix, uri);
    }

    @Override
    public Container createAlt() {
        return JenaObjects.container(getJenaObject().createAlt());
    }

    @Override
    public Container createBag() {
        return JenaObjects.container(getJenaObject().createBag());
    }

    @Override
    public Resource createList(Iterator<? extends RDFNode> members) {
        return JenaObjects.resource(getJenaObject().createList(new Iterator<org.apache.jena.rdf.model.RDFNode>() {
            @Override
            public boolean hasNext() {
                return members.hasNext();
            }

            @Override
            public org.apache.jena.rdf.model.RDFNode next() {
                return (org.apache.jena.rdf.model.RDFNode) ((JenaObject<?>) members.next()).getJenaObject();
            }
        }));
    }

    @Override
    public Container createSeq() {
        return JenaObjects.container(getJenaObject().createSeq());
    }

    @Override
    public Resource createResource() {
        return JenaObjects.resource(getJenaObject().createResource());
    }

    @Override
    public Resource createResource(String uri) {
        return JenaObjects.resource(getJenaObject().createResource(uri));
    }

    @Override
    public Resource createResource(String uri, Resource type) {
        return JenaObjects.resource(getJenaObject().createResource(uri, getJenaObject((ResourceImpl<?>) type)));
    }

    @Override
    public Resource createResource(Resource type) {
        return JenaObjects.resource(getJenaObject().createResource(getJenaObject((ResourceImpl<?>) type)));
    }

    @Override
    public Property createProperty(String uri) {
        return JenaObjects.property(getJenaObject().createProperty(uri));
    }

    @Override
    public Property createProperty(String nameSpace, String localName) {
        return JenaObjects.property(getJenaObject().createProperty(nameSpace, localName));
    }

    @Override
    public Literal createLiteral(String value) {
        return JenaObjects.literal(getJenaObject().createLiteral(value));
    }

    @SuppressWarnings("deprecation") // Jena 4.9 says "wellFormed" is ignored, but doesn't seem to be the case looking at implementation
    @Override
    public Literal createLiteral(String value, boolean wellFormed) {
        return JenaObjects.literal(getJenaObject().createLiteral(value, wellFormed));
    }

    @Override
    public Literal createTypedLiteral(Object value) {
        return JenaObjects.literal(getJenaObject().createTypedLiteral(value));
    }

    @Override
    public Literal createTypedLiteral(int value) {
        return JenaObjects.literal(getJenaObject().createTypedLiteral(value));
    }

    @Override
    public Literal createTypedLiteral(String lexicalForm, RDFDataType datatype) {
        // @formatter:off
        return JenaObjects.literal(getJenaObject().createTypedLiteral(
                lexicalForm,
               getJenaObject((RDFDataTypeImpl) datatype)
        ));
        // @formatter:on
    }

    @Override
    public Literal createTypedLiteral(Object value, RDFDataType datatype) {
     // @formatter:off
        return JenaObjects.literal(getJenaObject().createTypedLiteral(
                value,
                getJenaObject((RDFDataTypeImpl) datatype)
        ));
        // @formatter:on
    }

    @Override
    public Literal createTypedLiteral(String lexicalForm, String typeURI) {
        return JenaObjects.literal(getJenaObject().createTypedLiteral(lexicalForm, typeURI));
    }

    @Override
    public Statement createStatement(Resource subject, Property property, RDFNode object) {
        // @formatter:off
        return JenaObjects.statement(getJenaObject().createStatement(
                getJenaObject((ResourceImpl<?>) subject),
                getJenaObject((PropertyImpl) property),
                (org.apache.jena.rdf.model.RDFNode) getJenaObject((JenaObject<?>) object
        )));
        // @formatter:on
    }

    @Override
    public boolean contains(Statement statement) {
        return getJenaObject().contains(getJenaObject((StatementImpl) statement));
    }

    @Override
    public void add(Statement statement) {
        getJenaObject().add(getJenaObject((StatementImpl) statement));
    }

    @Override
    public void remove(Statement statement) {
        getJenaObject().remove(getJenaObject((StatementImpl) statement));
    }

    @Override
    public Resource getResource(String uri) {
        return JenaObjects.resource(getJenaObject().getResource(uri));
    }

    @Override
    public Property getProperty(String uri) {
        return JenaObjects.property(getJenaObject().getProperty(uri));
    }

    @Override
    public ExtendedIterator<Resource> listSubjectsWithProperty(Property property) {
        // @formatter:off
        return new ExtendedIteratorImpl<>(getJenaObject().listSubjectsWithProperty(
                getJenaObject((PropertyImpl) property)));
        // @formatter:on
    }

    @Override
    public ExtendedIterator<Resource> listSubjectsWithProperty(Property property, RDFNode object) {
        // @formatter:off
        return new ExtendedIteratorImpl<>(getJenaObject().listSubjectsWithProperty(
                getJenaObject((PropertyImpl) property),
                (org.apache.jena.rdf.model.RDFNode) getJenaObject((JenaObject<?>) object)));
        // @formatter:on

    }

    @Override
    public ExtendedIterator<Statement> listStatements() {
        return new ExtendedIteratorImpl<>(getJenaObject().listStatements());
    }

    @Override
    public ExtendedIterator<Statement> listStatements(Resource subject, Property property, RDFNode object) {
        // @formatter:off
        return new ExtendedIteratorImpl<>(getJenaObject().listStatements(
                getJenaObject((ResourceImpl<?>) subject),
                getJenaObject((PropertyImpl) property),
                (org.apache.jena.rdf.model.RDFNode) getJenaObject((JenaObject<?>) object)));
         // @formatter:on
    }

    @Override
    public ExtendedIterator<Statement> listStatements(Predicate<Statement> predicate) {
        return new ExtendedIteratorImpl<>(getJenaObject().listStatements().filterKeep( //
                statement -> predicate.test(JenaObjects.statement(statement))));
    }

    @SuppressWarnings({ "deprecation", "java:S1874" }) // ReifiedStatement is deprecated but still to be used (PRDOSLC-1165)
    @Override
    public ExtendedIterator<ReifiedStatement> listReifiedStatements() {
        return new ExtendedIteratorImpl<>(getJenaObject().listReifiedStatements());
    }

    @SuppressWarnings({ "deprecation", "java:S1874" }) // ReifiedStatement is deprecated but still to be used (PRDOSLC-1165)
    @Override
    public ExtendedIterator<ReifiedStatement> listReifiedStatements(Statement statement) {
        return new ExtendedIteratorImpl<>(getJenaObject().listReifiedStatements(((StatementImpl) statement).getJenaObject()));
    }

    @Override
    public boolean isIsomorphicWith(Model model) {
        return getJenaObject().isIsomorphicWith(getJenaObject((ModelImpl) model));
    }

    @Override
    public RDFReader getReader() {
        RDFReaderF factory = getJenaObject();
        return new RDFReaderImpl(factory.getReader());
    }

    @Override
    public RDFReader getReader(String lang) {
        RDFReaderF factory = getJenaObject();
        return new RDFReaderImpl(factory.getReader(lang));
    }

    @Override
    public RDFWriter getWriter() {
        RDFWriterF factory = getJenaObject();
        return new RDFWriterImpl(factory.getWriter());
    }

    @Override
    public RDFWriter getWriter(String lang) {
        RDFWriterF factory = getJenaObject();
        return new RDFWriterImpl(factory.getWriter(lang));
    }

}
