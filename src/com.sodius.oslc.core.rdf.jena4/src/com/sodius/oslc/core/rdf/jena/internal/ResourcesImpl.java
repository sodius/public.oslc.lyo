/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Resources;

public class ResourcesImpl implements Resources {

    @Override
    public Property RDF_FIRST() {
        return JenaObjects.property(RDF.first);
    }

    @Override
    public Property RDF_LI(int i) {
        return JenaObjects.property(RDF.li(i));
    }

    @Override
    public Resource RDF_NIL() {
        return JenaObjects.resource(RDF.nil);
    }

    @Override
    public Property RDF_PREDICATE() {
        return JenaObjects.property(RDF.predicate);
    }

    @Override
    public Property RDF_OBJECT() {
        return JenaObjects.property(RDF.object);
    }

    @Override
    public Property RDF_REST() {
        return JenaObjects.property(RDF.rest);
    }

    @Override
    public Resource RDF_SEQ() {
        return JenaObjects.resource(RDF.Seq);
    }

    @Override
    public Resource RDF_SUBJECT() {
        return JenaObjects.resource(RDF.subject);
    }

    @Override
    public Resource RDF_STATEMENT() {
        return JenaObjects.resource(RDF.Statement);
    }

    @Override
    public Property RDF_TYPE() {
        return JenaObjects.property(RDF.type);
    }

    @Override
    public Resource RDFS_CONTAINER() {
        return JenaObjects.resource(RDFS.Container);
    }

    @Override
    public Property RDFS_MEMBER() {
        return JenaObjects.property(RDFS.member);
    }

    @Override
    public Resource createResource(String uri) {
        return JenaObjects.resource(ResourceFactory.createResource(uri));
    }

    @Override
    public Property createProperty(String uri) {
        return JenaObjects.property(ResourceFactory.createProperty(uri));
    }
}
