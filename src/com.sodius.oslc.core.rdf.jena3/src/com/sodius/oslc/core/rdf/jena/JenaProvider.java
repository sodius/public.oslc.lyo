/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena;

import org.apache.jena.system.JenaSystem;

import com.sodius.oslc.core.rdf.Models;
import com.sodius.oslc.core.rdf.RDFDataTypes;
import com.sodius.oslc.core.rdf.Resources;
import com.sodius.oslc.core.rdf.jena.internal.ModelsImpl;
import com.sodius.oslc.core.rdf.jena.internal.RDFDataTypesImpl;
import com.sodius.oslc.core.rdf.jena.internal.ResourcesImpl;

@SuppressWarnings("deprecation")
public class JenaProvider {

    static {
        // Cannot use org.apache.jena.sys.JenaSystem, which doesn't exist in Jena 3.4, hence the deprecation warning
        JenaSystem.init();
    }

    private static final JenaProvider INSTANCE = new JenaProvider();

    public static Models models() {
        return INSTANCE.models;
    }

    public static Resources resources() {
        return INSTANCE.resources;
    }

    public static RDFDataTypes dataTypes() {
        return INSTANCE.dataTypes;
    }

    private final ModelsImpl models;
    private final Resources resources;
    private final RDFDataTypes dataTypes;

    private JenaProvider() {
        this.models = new ModelsImpl();
        this.resources = new ResourcesImpl();
        this.dataTypes = new RDFDataTypesImpl();
    }

}
