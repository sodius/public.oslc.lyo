/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import com.sodius.oslc.core.rdf.Literal;
import com.sodius.oslc.core.rdf.RDFDataType;

class LiteralImpl extends RDFNodeImpl<org.apache.jena.rdf.model.Literal> implements Literal {

    LiteralImpl(org.apache.jena.rdf.model.Literal literal) {
        super(literal);
    }

    @Override
    public String getString() {
        return getJenaObject().getString();
    }

    @Override
    public Object getValue() {
        return getJenaObject().getValue();
    }

    @Override
    public int getInt() {
        return getJenaObject().getInt();
    }

    @Override
    public RDFDataType getDatatype() {
        return JenaObjects.datatype(getJenaObject().getDatatype());
    }

    @Override
    public String getDatatypeURI() {
        return getJenaObject().getDatatypeURI();
    }

    @Override
    public boolean isWellFormedXML() {
        return getJenaObject().isWellFormedXML();
    }

    @Override
    public String getLanguage() {
        return getJenaObject().getLanguage();
    }

    @Override
    public String getLexicalForm() {
        return getJenaObject().getLexicalForm();
    }
}
