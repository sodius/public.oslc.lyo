/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import java.util.Calendar;

import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.datatypes.xsd.impl.XMLLiteralType;

import com.sodius.oslc.core.rdf.RDFDataType;
import com.sodius.oslc.core.rdf.RDFDataTypes;

public class RDFDataTypesImpl implements RDFDataTypes {

    @Override
    public RDFDataType XML_LITERAL() {
        return JenaObjects.datatype(XMLLiteralType.theXMLLiteralType);
    }

    @Override
    public RDFDataType XSD_DATE() {
        return JenaObjects.datatype(XSDDatatype.XSDdate);
    }

    @Override
    public RDFDataType XSD_DATE_TIME() {
        return JenaObjects.datatype(XSDDatatype.XSDdateTime);
    }

    @Override
    public RDFDataType XSD_DECIMAL() {
        return JenaObjects.datatype(XSDDatatype.XSDdecimal);
    }

    @Override
    public RDFDataType XSD_DOUBLE() {
        return JenaObjects.datatype(XSDDatatype.XSDdouble);
    }

    @Override
    public RDFDataType XSD_FLOAT() {
        return JenaObjects.datatype(XSDDatatype.XSDfloat);
    }

    @Override
    public String formatDate(Calendar calendar) {
        XSDDateTime valuec = new XSDDateTime(calendar);
        valuec.narrowType(XSDDatatype.XSDdate);
        String valueDate = valuec.toString();
        if (valueDate.endsWith("Z")) { //$NON-NLS-1$
            valueDate = valueDate.replace("Z", ""); //$NON-NLS-1$ //$NON-NLS-2$
        }
        return valueDate;
    }

    @Override
    public boolean isXsdDateTime(Object value) {
        return value instanceof XSDDateTime;
    }

    @Override
    public Calendar getXsdDateTimeAsCalendar(Object value) {
        return ((XSDDateTime) value).asCalendar();
    }

    @Override
    public Calendar getXsdDateTimeFromCalendar(Calendar calendar) {
        return new XSDDateTime(calendar).asCalendar();
    }

    @Override
    public RDFDataType getTypeByName(String uri) {
        return JenaObjects.datatype(TypeMapper.getInstance().getTypeByName(uri));
    }

}
