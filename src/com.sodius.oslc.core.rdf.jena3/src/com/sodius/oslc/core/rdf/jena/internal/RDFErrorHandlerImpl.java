/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import com.sodius.oslc.core.rdf.RDFErrorHandler;

public class RDFErrorHandlerImpl extends JenaObject<org.apache.jena.rdf.model.RDFErrorHandler> implements RDFErrorHandler {

    public RDFErrorHandlerImpl(org.apache.jena.rdf.model.RDFErrorHandler delegate) {
        super(delegate);
    }

    @Override
    public void warning(Exception e) {
        getJenaObject().warning(e);
    }

    @Override
    public void error(Exception e) {
        getJenaObject().error(e);
    }

    @Override
    public void fatalError(Exception e) {
        getJenaObject().fatalError(e);
    }

    static org.apache.jena.rdf.model.RDFErrorHandler toJenaObject(RDFErrorHandler handler) {
        if (handler instanceof RDFErrorHandlerImpl) {
            return ((RDFErrorHandlerImpl) handler).getJenaObject();
        } else {
            return new org.apache.jena.rdf.model.RDFErrorHandler() {

                @Override
                public void warning(Exception e) {
                    handler.warning(e);
                }

                @Override
                public void fatalError(Exception e) {
                    handler.fatalError(e);
                }

                @Override
                public void error(Exception e) {
                    handler.error(e);
                }
            };
        }
    }
}
