/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import com.sodius.oslc.core.rdf.AnonId;
import com.sodius.oslc.core.rdf.Container;
import com.sodius.oslc.core.rdf.Literal;
import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.Property;
import com.sodius.oslc.core.rdf.RDFDataType;
import com.sodius.oslc.core.rdf.ReifiedStatement;
import com.sodius.oslc.core.rdf.Resource;
import com.sodius.oslc.core.rdf.Statement;

class JenaObjects {

    static Object of(Object jenaObject) {
        if (jenaObject == null) {
            return null;
        }

        // model
        if (jenaObject instanceof org.apache.jena.rdf.model.Model) {
            return model((org.apache.jena.rdf.model.Model) jenaObject);
        }

        // resource (subclasses: property, reified statement)
        else if (jenaObject instanceof org.apache.jena.rdf.model.Resource) {
            return resource((org.apache.jena.rdf.model.Resource) jenaObject);
        }

        // statement
        else if (jenaObject instanceof org.apache.jena.rdf.model.Statement) {
            return statement((org.apache.jena.rdf.model.Statement) jenaObject);
        }

        // literal
        else if (jenaObject instanceof org.apache.jena.rdf.model.Literal) {
            return literal((org.apache.jena.rdf.model.Literal) jenaObject);
        }

        // datatype
        else if (jenaObject instanceof org.apache.jena.datatypes.RDFDatatype) {
            return datatype((org.apache.jena.datatypes.RDFDatatype) jenaObject);
        }

        // unsupported
        else {
            return jenaObject;
        }
    }

    static Model model(org.apache.jena.rdf.model.Model model) {
        return model == null ? null : new ModelImpl(model);
    }

    static Resource resource(org.apache.jena.rdf.model.Resource resource) {

        // container
        if (resource instanceof org.apache.jena.rdf.model.Container) {
            return container((org.apache.jena.rdf.model.Container) resource);
        }

        // property
        if (resource instanceof org.apache.jena.rdf.model.Property) {
            return property((org.apache.jena.rdf.model.Property) resource);
        }

        // reified statement
        if (resource instanceof org.apache.jena.rdf.model.ReifiedStatement) {
            return reifiedStatement((org.apache.jena.rdf.model.ReifiedStatement) resource);
        }

        return resource == null ? null : new ResourceImpl<>(resource);
    }

    static AnonId anonId(org.apache.jena.rdf.model.AnonId anonId) {
        return anonId == null ? null : new AnonIdImpl(anonId);
    }

    static Container container(org.apache.jena.rdf.model.Container container) {
        return container == null ? null : new ContainerImpl(container);
    }

    static Property property(org.apache.jena.rdf.model.Property property) {
        return property == null ? null : new PropertyImpl(property);
    }

    static Statement statement(org.apache.jena.rdf.model.Statement statement) {
        return statement == null ? null : new StatementImpl(statement);
    }

    static ReifiedStatement reifiedStatement(org.apache.jena.rdf.model.ReifiedStatement statement) {
        return statement == null ? null : new ReifiedStatementImpl(statement);
    }

    static Literal literal(org.apache.jena.rdf.model.Literal literal) {
        return literal == null ? null : new LiteralImpl(literal);
    }

    static RDFDataType datatype(org.apache.jena.datatypes.RDFDatatype datatype) {
        return datatype == null ? null : new RDFDataTypeImpl(datatype);
    }

    private JenaObjects() {
    }

}
