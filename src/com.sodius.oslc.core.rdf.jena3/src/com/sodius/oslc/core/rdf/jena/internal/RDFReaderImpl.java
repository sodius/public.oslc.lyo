/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import java.io.InputStream;
import java.io.Reader;

import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.RDFErrorHandler;
import com.sodius.oslc.core.rdf.RDFReader;

class RDFReaderImpl extends JenaObject<org.apache.jena.rdf.model.RDFReader> implements RDFReader {

    RDFReaderImpl(org.apache.jena.rdf.model.RDFReader delegate) {
        super(delegate);
    }

    @Override
    public void read(Model model, InputStream stream, String base) {
        getJenaObject().read(((ModelImpl) model).getJenaObject(), stream, base);
    }

    @Override
    public void read(Model model, Reader reader, String base) {
        getJenaObject().read(((ModelImpl) model).getJenaObject(), reader, base);
    }

    @Override
    public void setErrorHandler(RDFErrorHandler handler) {
        getJenaObject().setErrorHandler(RDFErrorHandlerImpl.toJenaObject(handler));
    }

    @Override
    public void setProperty(String propName, Object propValue) {
        getJenaObject().setProperty(propName, propValue);
    }

}
