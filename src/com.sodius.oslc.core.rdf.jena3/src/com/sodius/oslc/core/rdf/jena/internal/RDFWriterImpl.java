/*
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0, or the Eclipse Distribution License 1.0
 * which is available at http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * SPDX-License-Identifier: EPL-2.0 OR BSD-3-Clause
 */
package com.sodius.oslc.core.rdf.jena.internal;

import java.io.OutputStream;
import java.io.Writer;

import com.sodius.oslc.core.rdf.Model;
import com.sodius.oslc.core.rdf.RDFErrorHandler;
import com.sodius.oslc.core.rdf.RDFWriter;

class RDFWriterImpl extends JenaObject<org.apache.jena.rdf.model.RDFWriter> implements RDFWriter {

    RDFWriterImpl(org.apache.jena.rdf.model.RDFWriter delegate) {
        super(delegate);
    }

    @Override
    public void write(Model model, OutputStream out, String base) {
        getJenaObject().write(((ModelImpl) model).getJenaObject(), out, base);
    }

    @Override
    public void write(Model model, Writer out, String base) {
        getJenaObject().write(((ModelImpl) model).getJenaObject(), out, base);
    }

    @Override
    public void setErrorHandler(RDFErrorHandler handler) {
        getJenaObject().setErrorHandler(RDFErrorHandlerImpl.toJenaObject(handler));
    }

    @Override
    public void setProperty(String propName, Object propValue) {
        getJenaObject().setProperty(propName, propValue);
    }

}
